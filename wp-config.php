<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
define('WPLANG', 'ru_RU');
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'project' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '`<aLN,=z~SBYAn+_e,<qkRxnkYqz/Oli?yB..;g;[{=EUZ8Mm,3]Uk<|K*Qt~cLC' );
define( 'SECURE_AUTH_KEY',  'n]?x}H&&y=l6r@iM<gi)U~dK6~pwYm7T{E)}IPJd~[EOhds%Hu)Ewi}>@f-T4KI4' );
define( 'LOGGED_IN_KEY',    '<4x6/5sW9%MW#,~DSa9j>,FUow0v7J6+I&QS?UNf@Wx`+W*WWsw!EKXh?xd(3YDV' );
define( 'NONCE_KEY',        'cEj[+Tg3z!J[b2T.p0lOd53+k[%2r`<fxA]_[(z,3ow0Psu;=zU0=&F&M@h6d?F3' );
define( 'AUTH_SALT',        'lqRkGv I|McA1uJbW,|nxqr-kRWjMm2~/-Y2%S>[5hmAem`K.kpONY@qnzNMdsLC' );
define( 'SECURE_AUTH_SALT', 'w1jsF|y)IyQ/jeVLCq(5(I[C{0|?pScHI!e6k?A]5-pU%0}4t[(##H tv!ZSFRLW' );
define( 'LOGGED_IN_SALT',   '*r-hY/CKrD!GV<d|M3Mqx4eJkx]lGuF[M) *_ %JpH+o4w?kmy$*es~zqbM[`shM' );
define( 'NONCE_SALT',       '@r}oR%4Q^hFL~q9y&Sq?2-#.+h[r<7t}2GN1T9L+7i(pG<6,?qq5LZ:gt[+,4V{8' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
