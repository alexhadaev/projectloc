<?php
/**
 * Theme-template functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package geo-sites
 */

// Clear wp_head()
show_admin_bar( true );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_head', 'wp_shortlink_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_host_js' );
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

require_once ABSPATH . 'wp-admin/includes/image.php';

// здесь .mo файл должен лежать в папке lang, которая находится в папке где находится сам файл в котором вызывается эта строка
// Подключаем имеющийся файл .mo (название файла: ru_RU.mo или другое, зависит от локали)
//add_action( 'plugins_loaded', 'load_my_textdomain' );
//function load_my_textdomain(){
//	$mo_file_path = dirname(__FILE__) . '/languages/'. get_locale() . '.mo';
//	load_textdomain( 'mytranslate', $mo_file_path );
//}

add_action('after_setup_theme', 'true_load_theme_textdomain');
function true_load_theme_textdomain(){
	load_theme_textdomain( 'mytranslate', get_template_directory() . '/languages' );
}
// Custom post type
require get_template_directory() . '/includes/custom-post-types.php';

// Custom taxonomies
require get_template_directory() . '/includes/custom-taxonomies.php';

// Initialize theme default settings
require get_template_directory() . '/includes/theme-settings.php';

// Add custom fields
require get_template_directory() . '/includes/custom-fields.php';

// Disable support for comments and trackbacks in post types.
//require get_template_directory() . '/includes/disable-comments.php';

// Disable XML-RPC service.
require get_template_directory() . '/includes/disable-xmlrpc.php';

// Adjust theme assets.
require get_template_directory() . '/includes/assets.php';

// Adjust theme assets.
require get_template_directory() . '/includes/custom-login.php';

// Custom excerpt.
require get_template_directory() . '/includes/custom-excerpt.php';

// Customize theme options
require get_template_directory() . '/includes/current-url.php';

// Customize theme options
require get_template_directory() . '/includes/customize-theme-options.php';

// Load more posts via ajax
require get_template_directory() . '/layouts/load-more-posts.php';

// Load more videos via ajax
require get_template_directory() . '/layouts/load-more-videos.php';

// Send article via ajax
require get_template_directory() . '/layouts/send-article.php';

// Fetch video data via ajax
require get_template_directory() . '/includes/fetch-video-data.php';

// Load wp-header for vBulletin
require get_template_directory() . '/vb-header.php';

// Load wp-footer for vBulletin
require get_template_directory() . '/vb-footer.php';

// Load breadcrumbs
require get_template_directory() . '/includes/breadcrumbs.php';

//upload acf post
//require get_template_directory() . '/layouts/acf-ajax.php';


## Сортировка подпунктов меню
function cmp($a, $b)//($b, $a)=desc
{
	if($a->menu_item_parent !== '0' && $b->menu_item_parent !== '0')
	return strcmp($a->title, $b->title);
}

add_filter( 'wp_nav_menu_objects', 'change_nav_menu_objects', 10, 2 );
function change_nav_menu_objects( $sorted_menu_items, $args ) {
	usort($sorted_menu_items, "cmp");
	return $sorted_menu_items;
}

//************** Добавить пункт меню
add_filter( 'wp_nav_menu_items', 'change_nav_menu_items', 10, 2 );

function change_nav_menu_items( $items, $args ) {
	if ( 'header-menu' == $args->theme_location ) {
		$items .= '<li class="menu-item">' . get_search_form( false ) . '</li>';
	}
	return $items;
}

//******************* шаблонизатор wp
function lw_comment_templates() {
	include_once get_stylesheet_directory() . "/templates/comments-templates.php";
}
add_action( "wp_footer", "lw_comment_templates" );
//var_dump(get_stylesheet_directory() . "/templates/comments-templates.php");

wp_localize_script( 'wp-api', 'wpApiSettings', array(
	'root' => esc_url_raw( rest_url() ),
	'nonce' => wp_create_nonce( 'wp_rest' )
) );

//включение комментариев для страниц по умолчанию start
function wph_enable_comments_pages($status, $post_type, $comment_type) {
	if ('page' === $post_type) {
		if (in_array($comment_type, array('pingback', 'trackback'))) {
			$status = get_option('default_ping_status');
		} else {
			$status = get_option('default_comment_status');
		}
	}
	return $status;
}
add_filter('get_default_comment_status', 'wph_enable_comments_pages', 10, 3);
//включение комментариев для страниц по умолчанию end


//acf_register_form( array(
//	'id'           => 'new-event',
//	'post_id'      => false,
//	'post_title'   => true,
//) );
//
//$meta_args = array(
//	'type'         => 'string',
//	'description'  => 'A meta key associated with a string meta value.',
////	'single'       => true,
//	'show_in_rest' => true,
//);
//register_meta( 'post', 'group', $meta_args );

add_action( 'rest_api_init', 'create_api_posts_meta_field' );

function create_api_posts_meta_field() {

	// register_rest_field ( 'name-of-post-type', 'name-of-field-to-return', array-of-callbacks-and-schema() )
	register_rest_field( 'post', 'post-meta-fields', array(
			'get_callback'    => 'get_post_meta_for_api',
			'schema'          => null,
		)
	);
}

function get_post_meta_for_api( $object ) {
	//get the id of the post object array
	$post_id = $object['id'];

	//return the post meta
	$field = get_post_meta( $post_id);
	return $field;
}

//**********************************************************************************************
add_shortcode( 'baztag', 'baztag_func' );
function baztag_func( $atts ) {
	return '<h1>Test Shortcode</h1>';
}

// add widget
require get_template_directory() . '/includes/widget.php';

function register_my_widgets(){
	register_sidebar( array(
		'name' => "Правая боковая панель сайта",
		'id' => 'right-sidebar',
		'description' => 'Эти виджеты будут показаны в правой колонке сайта',
		'before_title' => '<h2>',
		'after_title' => '</h2>'
	) );
}
add_action( 'widgets_init', 'register_my_widgets' );


