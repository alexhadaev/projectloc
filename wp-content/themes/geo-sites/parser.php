<?php
/*  Template Name: Parser */

/**
 *
 * Evaluate text readability
 *
 * Where to include this function?
 * In WordPress: theme functions.php file
 * ... or wherever you are keeping your site functions
 *
 * Where to use?
 * In WordPress: theme single.php file (post view template)
 * ... or wherever you want to display it.
 *
 * How to display it:
 * <?php echo cn_article_readability($text); ?>
 * <?php echo cn_article_readability($text, false); ?> -> if you donвЂ™t want to exclude <pre></pre> snippets
 * WordPress example:
 * <?php echo cn_article_readability($post->post_content); ?>
 *
 * BTW, this software is free.
 * by Creative Nights <http://creativenights.com/>
 *
 */


//function cn_article_readability($text, $remove_PRE = true) {
//
//	if ($remove_PRE) {
//		$text = preg_replace('/pre>(.*)?<\/pre/si','', $text);
//	}
//
//	function countSentences($x){
//		$find = array('Mr.', 'Mrs.', 'Ms.', 'i.e.', 'e.g.', 'vs.');
//		$x = str_replace($find,'',$x);
//		return preg_match_all('/[^\s](\.|\!|\?)(?!\w)/', $x, $match);
//	}
//
//
//	// Based on http://mikebrum.com/counting-the-syllables-in-a-word-with-php/
//
//	function countSyllables($x) {
//		$triples = 'dn\вЂ™t|dn\'t|eau|iou|ouy|you|bl\s';
//		$doubles = 'ai|ae|ay|au|ea|ee|ei|eu|ey|ie|ii|io|oa|oe|oi|oo|ou|oy|ue|uy|ya|ye|yi|yo|yu';
//		$singles = 'a|e|i|o|u|y';
//
//		// Cleaning up word endings
//		$find = array('s ','e ');
//		$x = str_replace($find, '', $x);
////		var_dump(preg_match_all('/(' . $triples . '|' . $doubles . '|' . $singles . ')/i', $x, $match));
//		return preg_match_all('/(' . $triples . '|' . $doubles . '|' . $singles . ')/i', $x, $match);
//	}
//
//
//	$find = array(' ','.','!','?',"\n");
//
//	$characters = strlen(str_replace($find, '', $text));
//	$syllables = countSyllables($text);
//	$words = str_word_count($text);
//	$sentences = countSentences($text);
//
//	$mins = floor($words / 200);
//	$secs = floor($words % 200 / (200 / 60));
//
//
//	// Formulas
//
//	$reading_time = $mins . ' minute' . ($mins == 1 ? '' : 's') . ', ' . $secs . ' second' . ($secs == 1 ? '' : 's');
//
//
//	// http://en.wikipedia.org/wiki/Automated_Readability_Index
//
//	$readability_index = (4.71 * ($characters/$words)) + (0.5 * ($words/$sentences)) - 21.43;
//	$readability_index = number_format($readability_index,2);
//
//
//	// http://en.wikipedia.org/wiki/Flesch-Kincaid_Readability_Test
//
//	$reading_ease_score = (206.835 - 1.015*($words/$sentences) - 84.6*($syllables/$words));
//	$reading_ease_score = number_format($reading_ease_score,2);
//
//
//	// Output everything
//
//	$output = '';
//
//	// $output .= $text;
//
//	$output .= '<p><strong>Estimated reading time</strong><br />';
//	$output .= $reading_time . '</p>';
//
//	$output .= '<p><strong>Automated readability index</strong><br />';
//	$output .= $readability_index . ' (lower is better)</p>';
//
//	$output .= '<p><strong>The Flesch-Kincaid reading ease</strong><br />';
//	$output .= $reading_ease_score . ' out of 120 (higher is better)</p>';
//
//	$output .= '<table class="article-stats"><tbody>';
//	$output .= '<tr><th>Characters</th><td>' . $characters . '</td></tr>';
//	$output .= '<tr><th>Syllables</th><td>' . $syllables . '</td></tr>';
//	$output .= '<tr><th>Words</th><td>' . $words . '</td></tr>';
//	$output .= '<tr><th>Sentences</th><td>' . $sentences . '</td></tr>';
//	$output .= '</tbody></table>';
//
//	return $output;
//
//}
//$text = "The package contains patterns for the following languages.";
//echo cn_article_readability($text);
//
//
?>
<form action="/parser/" method="get">
	<input type="text" name="site_url">
    <textarea name="site_text"></textarea>
	<input type="submit">
</form>
<?php
if ($_GET['site_url'] != '' || $_GET['site_text'] != ''){
	var_dump('url = ', $_GET['site_url']);
	var_dump('text = ', $_GET['site_text']);
	require_once 'redability.php';
}


