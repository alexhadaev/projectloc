<?
function getGunningIndex($sentenceCount, $polysyllables, $wordCount) {
    $percentagePolysyllables = 100 * ($polysyllables / $wordCount);
	$averageSentenceLength = $wordCount / $sentenceCount;
    $gunning = 0.4 * ($averageSentenceLength + $percentagePolysyllables);
    return $gunning;
}

function getFleschScore($wordCount, $sentenceCount, $syllableCount) {
    $wordsToSentences = 1.015 * ($wordCount / $sentenceCount);
    $syllablesToWords = 84.6 * ($syllableCount / $wordCount);
    $flesch = 206.835 - $wordsToSentences - $syllablesToWords;

    $grade = "0";

    if ($flesch >= 90) {
        $grade = 5;
    }
    elseif ($flesch >= 80) {
        $grade = 6;
    }
    elseif ($flesch >= 70) {
        $grade = 7;
    }
    elseif ($flesch >= 65)
    {
        $grade = 8;
    }
    elseif ($flesch >= 60) {
        $grade = 9;
    }
    elseif ($flesch >= 57) {
        $grade = 10;
    }
    elseif ($flesch >= 54) {
        $grade = 11;
    }
    elseif ($flesch >= 50) {
        $grade = 12;
    }
    elseif ($flesch >= 40) {
        $grade = 13;
    }
    elseif ($flesch >= 30) {
        $grade = 14;
    }
    elseif ($flesch >= 15) {
        $grade = 15;
    }
    else {
        $grade = 16;
    }
    return [
        'score' => $flesch,
        'grade' => $grade
    ];
}

function getFleschKincaid($wordCount, $sentenceCount, $syllableCount) {
    $wordsToSentences = 0.39 * ($wordCount / $sentenceCount);
    $syllablesToWords = 11.8 * ($syllableCount / $wordCount);
    $fleschKincaid = $wordsToSentences + $syllablesToWords - 15.59;

    return $fleschKincaid;
}

function getSmog($polysyllables, $sentenceCount) {
    $squareRoot = sqrt( 30 * $polysyllables / $sentenceCount);
    $smog = 1.0430 * $squareRoot + 3.1291;

    return $smog;
}

function getColemanLiau($characterCount, $sentenceCount, $wordCount) {
    $lettersToWords = 0.0588 * ($characterCount / $wordCount * 100);
    $sentencesToWords = 0.296 * ($sentenceCount / $wordCount * 100);
    $colemanLiauGrade = $lettersToWords - $sentencesToWords - 15.8;
    return $colemanLiauGrade;
}

function getAri($characterCount, $wordCount, $sentenceCount) {
    $charactersToWords = 4.71 * ($characterCount / $wordCount);
    $wordsToSentences = 0.5 * ($wordCount / $sentenceCount);
    $ari = $charactersToWords + $wordsToSentences - 21.43;
    return $ari;
}

