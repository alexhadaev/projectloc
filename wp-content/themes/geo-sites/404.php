<?php
/**
 * Template Name: Error Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package geo-sites
 */

get_header(); ?>

  <div class="error-page container">
    <div class="row">
      <main class="main">
        <div class="error-code">404</div>
        <h1 class="error-title"><?php _e('Page Not Found', 'mytranslate');?></h1>
        <p class="error-description">The page you are looking was move, removed, renamed or might never existed.</p>
      </main>
      <aside class="aside">
        <?php get_template_part( 'layouts/aside-menu-home' ); ?>
      </aside>
    </div>
  </div>

<?php get_footer();
