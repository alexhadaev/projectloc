<?php
/**
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package geo-sites
 */

get_header();
$currentPage = get_query_var('paged');
?>

	<div class="category-page container">
		<div class="row">
			<main class="main">
				<h2 class="section-title"><span><?= the_title(); ?></span></h2>
				<?php
				$args = array(
					'post_status' => 'publish',
					'posts_per_page' => get_option('posts_per_page'),
					'paged' => $currentPage,
				);
				$posts = new WP_Query($args); ?>
				<?php if ($posts->have_posts()) : ?>
					<ul class="row list">
						<?php /* Start the Loop */
						while ($posts->have_posts()) : $posts->the_post();
							get_template_part('layouts/preview-post');
						endwhile; ?>
					</ul>
					<div class="pagination">
						<?php echo paginate_links(array(
								'total' => $posts->max_num_pages,
								'prev_text' => '<span> < </span><span>' . __('Prev', '') . '</span>',
								'next_text' => '<span>' . __('Next', '') . '</span><span> > </span>',
								'mid_size' => 5,
							)
						); ?>
					</div>
					<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			</main>
			<aside class="aside">
				<?php get_template_part('layouts/aside-categories'); ?>
				<?php get_template_part( 'layouts/booking-widget' ); ?>
				<?php get_template_part( 'layouts/advertisement-aside' ); ?>
			</aside>
		</div>
	</div>

<?php get_footer();
