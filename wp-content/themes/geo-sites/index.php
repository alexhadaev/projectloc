<?php
/**
 * Template Name: Index
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package geo-sites
 */

get_header(); ?>
    <div class="home-page container">
        <div class="row">
            <main class="main">
                <?php get_template_part( 'layouts/booking-widget' ); ?>
                <?php get_template_part( 'layouts/section-latest-posts' ); ?>
                <?php get_template_part( 'layouts/advertisement-content' ); ?>
                <?php get_template_part( 'layouts/section-random-pages' ); ?>
                <?php get_template_part( 'layouts/advertisement-content' ); ?>
                <?php get_template_part( 'layouts/section-random-videos' ); ?>
                <?php get_template_part( 'layouts/advertisement-content' ); ?>
            </main>
            <aside class="aside">
                <?php get_template_part( 'layouts/advertisement-aside' ); ?>
                <?php get_template_part( 'layouts/aside-menu-home' ); ?>
            </aside>
        </div>
    </div>

<?php get_footer();
