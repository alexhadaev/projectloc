<?php
/**
 * The template for displaying tag pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package geo-sites
 */
get_header(); ?>
  <div class="tag-page container">
    <div class="row">
      <main class="main">
		  <?php if ( have_posts() ) : the_archive_title( '<h1 class="section-title"><span>', '</span></h1>' ); ?>
        <ul class="row pages-list">
          <?php while ( have_posts() ) : the_post();
            get_template_part( 'layouts/preview', get_post_type() );
          endwhile; ?>
        </ul>
        <div class="pagination">
          <?php echo paginate_links( array(
              'prev_text' => '<span> < </span><span>' . __( 'Prev', '' ) . '</span>',
              'next_text' => '<span>' . __( 'Next', '' ) . '</span><span> > </span>',
              'mid_size'  => 5,
            )
          ); ?>
        </div>
		  <?php endif; ?>
      </main><!-- #main -->
      <aside class="aside">
        <?php get_template_part( 'layouts/booking-widget' ); ?>
        <?php get_template_part( 'layouts/advertisement-aside' ); ?>
      </aside>
    </div>
  </div>
<?php
get_footer();