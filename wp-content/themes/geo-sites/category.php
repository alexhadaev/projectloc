<?php
/**
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package geo-sites
 */

get_header();
$category_id = get_query_var('cat');
$currentPage = get_query_var('paged');
?>

  <div class="category-page container">
    <div class="row">
      <main class="main">
        <h2 class="section-title"><span><?= get_cat_name( $category_id ) ?></span></h2>
        <?php

//        $page = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $args = array(
          'cat' => $category_id,
          'post_status' => 'publish',
          'posts_per_page' => 2,
          'paged'=>$page
        );
        query_posts($args); ?>
        <?php if (have_posts()) : ?>
          <ul class="row list">
            <?php /* Start the Loop */
            while (have_posts()) : the_post();
            the_title();
            echo '<br>';
//              get_template_part('layouts/preview-post');
            endwhile; ?>
          </ul>
          <div class="pagination">
            <?php echo paginate_links(array(
//                'total' => $posts->max_num_pages,
                'prev_text' => '<span> < </span><span>' . __('Prev', '') . '</span>',
                'next_text' => '<span>' . __('Next', '') . '</span><span> > </span>',
                'mid_size' => 2,
              )
            ); ?>
          </div>
          <?php wp_reset_postdata(); ?>
        <?php endif; ?>
      </main>
      <aside class="aside">
        <?php get_template_part('layouts/aside-categories'); ?>
        <?php get_template_part( 'layouts/booking-widget' ); ?>
        <?php get_template_part( 'layouts/advertisement-aside' ); ?>
      </aside>
    </div>
  </div>

<?php get_footer();
