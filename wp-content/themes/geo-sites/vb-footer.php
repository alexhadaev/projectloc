<?php
/*
 * Load wp-footer for vBulletin
 *****************************************************************************************/

function load_footer() { ?>
  <style><?php include 'vb-footer.css'; ?></style>
  <div class="wp-footer">
    <div class="vb-container">
      <div class="top-block">
        <!-- The WordPress Menu goes here -->
        <?php wp_nav_menu(
          array(
            'theme_location' => 'footer-menu',
            'menu_class'     => 'navbar-nav',
            'fallback_cb'    => '',
          ) ); ?>
      </div>
      <p class="copyright"><?php echo get_theme_mod( 'copyright-text' ) ?></p>
    </div>
  </div>
  <?php die();
}

add_action( 'wp_ajax_load_footer', 'load_footer' );
add_action( 'wp_ajax_nopriv_load_footer', 'load_footer' );