<?php

/**
 * Register news taxonomies.
 */
function create_custom_taxonomies()
{
  // Add authors taxonomy
  $labels = array(
    'name' => _x('Journalists', 'taxonomy general name', 'geo-sites'),
    'singular_name' => _x('journalist', 'taxonomy singular name', 'geo-sites'),
    'search_items' => __('Search journalists', 'geo-sites'),
    'all_items' => __('All journalists', 'geo-sites'),
    'edit_item' => __('Edit journalist', 'geo-sites'),
    'update_item' => __('Update journalist', 'geo-sites'),
    'add_new_item' => __('Add new journalist', 'geo-sites'),
    'new_item_name' => __('New journalist', 'geo-sites'),
    'menu_name' => __('Journalists', 'geo-sites'),
  );

  $args = array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'journalist'),
  );
  register_taxonomy('journalist', array('news'), $args);

  // Add sources taxonomy
  $labels = array(
    'name' => _x('Sources', 'taxonomy general name', 'geo-sites'),
    'singular_name' => _x('Source', 'taxonomy singular name', 'geo-sites'),
    'search_items' => __('Search sources', 'geo-sites'),
    'all_items' => __('All sources', 'geo-sites'),
    'edit_item' => __('Edit source', 'geo-sites'),
    'update_item' => __('Update source', 'geo-sites'),
    'add_new_item' => __('Add new source', 'geo-sites'),
    'new_item_name' => __('New source', 'geo-sites'),
    'menu_name' => __('Sources', 'geo-sites'),
  );
  $args = array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'source'),
    'meta_box_cb' => 'post_categories_meta_box',
  );
  register_taxonomy('source', array('news'), $args);

  // Add podcast taxonomy
  $labels = array(
    'name' => _x('Podcasts', 'taxonomy general name', 'geo-sites'),
    'singular_name' => _x('Podcast', 'taxonomy singular name', 'geo-sites'),
    'search_items' => __('Search podcasts', 'geo-sites'),
    'all_items' => __('All podcasts', 'geo-sites'),
    'edit_item' => __('Edit podcast', 'geo-sites'),
    'update_item' => __('Update podcast', 'geo-sites'),
    'add_new_item' => __('Add new podcast', 'geo-sites'),
    'new_item_name' => __('New podcast', 'geo-sites'),
    'menu_name' => __('Podcasts', 'geo-sites'),
  );
  $args = array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'podcast'),
    'meta_box_cb' => 'post_categories_meta_box',
  );
  register_taxonomy('podcast', array('videos'), $args);

  // Add language taxonomy
  $labels = array(
    'name' => _x('Languages', 'taxonomy general name', 'geo-sites'),
    'singular_name' => _x('Language', 'taxonomy singular name', 'geo-sites'),
    'search_items' => __('Search language', 'geo-sites'),
    'all_items' => __('All languages', 'geo-sites'),
    'edit_item' => __('Edit language', 'geo-sites'),
    'update_item' => __('Update language', 'geo-sites'),
    'add_new_item' => __('Add new language', 'geo-sites'),
    'new_item_name' => __('New language', 'geo-sites'),
    'menu_name' => __('Languages', 'geo-sites'),
  );
  $args = array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array('slug' => 'language'),
    'meta_box_cb' => 'post_categories_meta_box',
  );
  register_taxonomy('language', array('blog'), $args);
}

add_action('init', 'create_custom_taxonomies', 0);


// Hide parent dropdown select for sources
function hide_parent_dropdown_select($args)
{
  if ('source' == $args['taxonomy']) {
    $args['echo'] = false;
  }
  return $args;
}

add_filter('post_edit_category_parent_dropdown_args', 'hide_parent_dropdown_select');

// Converting term IDs to term names
function convert_taxonomy_terms_to_integers()
{
  $taxonomies = ['source', 'podcast', 'language'];

  foreach ($taxonomies as $taxonomy) {
    if (isset($_POST['tax_input'][$taxonomy]) && is_array($_POST['tax_input'][$taxonomy])) {
      $terms = $_POST['tax_input'][$taxonomy];
      $new_terms = array_map('intval', $terms);
      $_POST['tax_input'][$taxonomy] = $new_terms;
    }
  }
}

add_action('admin_init', 'convert_taxonomy_terms_to_integers');
