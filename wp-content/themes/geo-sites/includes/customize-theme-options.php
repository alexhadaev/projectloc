<?php

/**
 * Register customize theme options.
 */

/**
 * Register advertisement.
 */
add_action('customize_register', function($customizer){
  $customizer->add_section(
    'advertisement',
    array(
      'title' => 'Advertisement',
      'priority' => 22,
    )
  );

  // Add scripts for 728x90 banner
  $customizer->add_setting(
    'ad_728x90',
    array('default' => '')
  );

  $customizer->add_control(
    'ad_728x90',
    array(
      'label' => 'Scripts for 728x90 banner',
      'section' => 'advertisement',
      'type' => 'textarea',
    )
  );

  // Add scripts for 300x250 banner
  $customizer->add_setting(
    'ad_300x250',
    array('default' => '')
  );

  $customizer->add_control(
    'ad_300x250',
    array(
      'label' => 'Scripts for 300x250 banner',
      'section' => 'advertisement',
      'type' => 'textarea',
    )
  );

  // Add scripts for booking widget
  $customizer->add_setting(
    'booking_widget',
    array('default' => '')
  );

  $customizer->add_control(
    'booking_widget',
    array(
      'label' => 'Scripts for booking widget',
      'section' => 'advertisement',
      'type' => 'textarea',
    )
  );
});



/**
 * Register socials.
 */
add_action('customize_register', function($customizer){
  $customizer->add_section(
    'social_media',
    array(
      'title' => 'Social media',
      'priority' => 22,
    )
  );

  // Add a header scripts
  $customizer->add_setting(
    'facebook',
    array('default' => '')
  );

  $customizer->add_control(
    'facebook',
    array(
      'label' => 'Link to Facebook account',
      'section' => 'social_media',
      'type' => 'text',
    )
  );

  // Add a head scripts
  $customizer->add_setting(
    'twitter',
    array('default' => '')
  );

  $customizer->add_control(
    'twitter',
    array(
      'label' => 'Link to Twitter account',
      'section' => 'social_media',
      'type' => 'text',
    )
  );
});


/**
 * Register scripts.
 */
add_action('customize_register', function($customizer){
  $customizer->add_section(
    'additional_data',
    array(
      'title' => 'Scripts',
      'priority' => 22,
    )
  );

	// Add a header scripts
  $customizer->add_setting(
    'gmap-api-code',
    array('default' => '')
  );

  $customizer->add_control(
    'gmap-api-code',
    array(
      'label' => 'Google Map API code',
      'section' => 'additional_data',
      'type' => 'text',
    )
  );

  // Add a header scripts
  $customizer->add_setting(
    'cx-code',
    array('default' => '')
  );

  $customizer->add_control(
    'cx-code',
    array(
      'label' => 'Google CSE cx-code',
      'section' => 'additional_data',
      'type' => 'text',
    )
  );

  // Add a head scripts
  $customizer->add_setting(
    'head-scripts',
    array('default' => '')
  );

  $customizer->add_control(
    'head-scripts',
    array(
      'label' => 'Head scripts',
      'section' => 'additional_data',
      'type' => 'textarea',
    )
  );

  // Add a footer scripts
  $customizer->add_setting(
    'footer-scripts',
    array('default' => '')
  );

  $customizer->add_control(
    'footer-scripts',
    array(
      'label' => 'Footer scripts ',
      'section' => 'additional_data',
      'type' => 'textarea',
    )
  );
});


/**
 * Register GMap.
 */
add_action('customize_register', function($customizer){
	$customizer->add_section(
		'city-guide-map',
		array(
			'title' => 'Map settings',
			'priority' => 23,
		)
	);

	// Add a map center latitude
	$customizer->add_setting(
		'map-center-latitude',
		array('default' => '')
	);

	$customizer->add_control(
		'map-center-latitude',
		array(
			'label' => 'Map center latitude',
			'section' => 'city-guide-map',
			'type' => 'text',
		)
	);

	// Add a map center longitude
	$customizer->add_setting(
		'map-center-longitude',
		array('default' => '')
	);

	$customizer->add_control(
		'map-center-longitude',
		array(
			'label' => 'Map center longitude',
			'section' => 'city-guide-map',
			'type' => 'text',
		)
	);

	// Add a map zoom
	$customizer->add_setting(
		'map-zoom',
		array('default' => '')
	);

	$customizer->add_control(
		'map-zoom',
		array(
			'label' => 'Map zoom',
			'section' => 'city-guide-map',
			'type' => 'text',
		)
	);
});


/**
 * Register SEO-meta.
 */
add_action('customize_register', function($customizer){
  $customizer->add_section(
    'seo-meta',
    array(
      'title' => 'SEO',
      'priority' => 24,
    )
  );

  // Add a front-page title
  $customizer->add_setting(
    'front-page-title',
    array('default' => '')
  );

  $customizer->add_control(
    'front-page-title',
    array(
      'label' => 'Front-page title',
      'section' => 'seo-meta',
      'type' => 'text',
    )
  );

  // Add a front-page keywords
  $customizer->add_setting(
    'front-page-keywords',
    array('default' => '')
  );

  $customizer->add_control(
    'front-page-keywords',
    array(
      'label' => 'Front-page keywords',
      'section' => 'seo-meta',
      'type' => 'text',
    )
  );

  // Add a front-page description
  $customizer->add_setting(
    'front-page-description',
    array('default' => '')
  );

  $customizer->add_control(
    'front-page-description',
    array(
      'label' => 'Front-page description',
      'section' => 'seo-meta',
      'type' => 'textarea',
    )
  );

  // Add a front-page description
  $customizer->add_setting(
    'front-page-image',
    array('default' => '')
  );

  $customizer->add_control( new WP_Customize_Image_Control($customizer, 'front-page-image', array(
    'label'    => __('Default image', ''),
    'section'  => 'seo-meta',
    'settings' => 'front-page-image',
  )));

  // Add a post-archive title
  $customizer->add_setting(
    'post-archive-title',
    array('default' => '')
  );

  $customizer->add_control(
    'post-archive-title',
    array(
      'label' => 'Post archive title',
      'section' => 'seo-meta',
      'type' => 'text',
    )
  );

  // Add a post-archive keywords
  $customizer->add_setting(
    'post-archive-keywords',
    array('default' => '')
  );

  $customizer->add_control(
    'post-archive-keywords',
    array(
      'label' => 'Post archive keywords',
      'section' => 'seo-meta',
      'type' => 'text',
    )
  );

  // Add a post-archive description
  $customizer->add_setting(
    'post-archive-description',
    array('default' => '')
  );

  $customizer->add_control(
    'post-archive-description',
    array(
      'label' => 'Post archive description',
      'section' => 'seo-meta',
      'type' => 'textarea',
    )
  );

  // Add a video-archive title
  $customizer->add_setting(
    'video-archive-title',
    array('default' => '')
  );

  $customizer->add_control(
    'video-archive-title',
    array(
      'label' => 'Video archive title',
      'section' => 'seo-meta',
      'type' => 'text',
    )
  );

  // Add a post-archive keywords
  $customizer->add_setting(
    'video-archive-keywords',
    array('default' => '')
  );

  $customizer->add_control(
    'video-archive-keywords',
    array(
      'label' => 'Video archive keywords',
      'section' => 'seo-meta',
      'type' => 'text',
    )
  );

  // Add a video-archive description
  $customizer->add_setting(
    'video-archive-description',
    array('default' => '')
  );

  $customizer->add_control(
    'video-archive-description',
    array(
      'label' => 'Video archive description',
      'section' => 'seo-meta',
      'type' => 'textarea',
    )
  );

  /**
   * Footer
   */
  $customizer->add_section(
    'footer',
    array(
      'title'       => esc_html__( 'Footer', '' ),
      'description' => esc_html__( 'Setting by footer', '' ),
      'priority'    => 160,
    )
  );

  $customizer->add_setting(
    'copyright-text'
  );

  $customizer->add_control(
    'copyright-text',
    array(
      'label'   => esc_html__( 'Text for footer - conclusion : ', '' ),
      'section' => 'footer',
      'type'    => 'textarea',
    )
  );

  $customizer->add_setting( 'footer-img-1' );
  $customizer->add_control(
    new WP_Customize_Image_Control(
      $customizer,
      'footer-image-1',
      array(
        'label'    => esc_html__( 'Image by footer:', '' ),
        'section'  => 'footer',
        'settings' => 'footer-img-1'
      )
    )
  );
  $customizer->add_setting( 'footer-img-2' );
  $customizer->add_control(
    new WP_Customize_Image_Control(
      $customizer,
      'footer-image-2',
      array(
        'label'    => esc_html__( 'Image by footer:', '' ),
        'section'  => 'footer',
        'settings' => 'footer-img-2'
      )
    )
  );

  $customizer->add_setting( 'footer-img-3' );
  $customizer->add_control(
    new WP_Customize_Image_Control(
      $customizer,
      'footer-image-3',
      array(
        'label'    => esc_html__( 'Image by footer:', '' ),
        'section'  => 'footer',
        'settings' => 'footer-img-3'
      )
    )
  );

  $customizer->add_setting( 'footer-img-4' );
  $customizer->add_control(
    new WP_Customize_Image_Control(
      $customizer,
      'footer-image-4',
      array(
        'label'    => esc_html__( 'Image by footer:', '' ),
        'section'  => 'footer',
        'settings' => 'footer-img-4'
      )
    )
  );
  $customizer->add_setting( 'footer-img-5' );
  $customizer->add_control(
    new WP_Customize_Image_Control(
      $customizer,
      'footer-image-5',
      array(
        'label'    => esc_html__( 'Image by footer:', '' ),
        'section'  => 'footer',
        'settings' => 'footer-img-5'
      )
    )
  );
  $customizer->add_setting( 'footer-img-6' );
  $customizer->add_control(
    new WP_Customize_Image_Control(
      $customizer,
      'footer-image-6',
      array(
        'label'    => esc_html__( 'Image by footer:', '' ),
        'section'  => 'footer',
        'settings' => 'footer-img-6'
      )
    )
  );

});