<?php
/**
 * Get current url
 */
function current_url() {
  if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
    $protocol = 'https://';
  } else {
    $protocol = 'http://';
  }
  $current_url=$protocol.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

  return $current_url;
}