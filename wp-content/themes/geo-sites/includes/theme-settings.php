<?php
/**
 * Check and setup theme's default settings
 */

// Disable Gutenberg for page
function disable_gutenberg($is_enabled, $post_type) {
  if ($post_type === 'page') return false; // change book to your post type

  return $is_enabled;
}
add_filter('use_block_editor_for_post_type', 'disable_gutenberg', 10, 2);

// Add custom header support
add_theme_support( 'custom-header' );

// Add thumbnails support
add_theme_support( 'post-thumbnails' );

// Add excerpt support to post-types
add_post_type_support( 'page', 'excerpt' );
add_post_type_support( 'videos', 'excerpt' );
//add_post_type_support( 'news', 'excerpt' );
add_post_type_support( 'blog', 'excerpt' );

// Add taxonomy support to post types
function add_taxonomy_support() {
	register_taxonomy_for_object_type( 'post_tag', 'page' );
  register_taxonomy_for_object_type( 'post_tag', 'news' );
  register_taxonomy_for_object_type( 'post_tag', 'blog' );
  register_taxonomy_for_object_type( 'post_tag', 'report' );
  register_taxonomy_for_object_type('category', 'news');
}

add_action( 'init', 'add_taxonomy_support' );

// Ensure all tags are included in queries
function tags_support_query( $wp_query ) {
	if ( $wp_query->get( 'tag' ) ) {
		$wp_query->set( 'post_type', 'any' );
	}
}

add_action( 'pre_get_posts', 'tags_support_query' );

function register_menus() {
	register_nav_menus(
		array(
			'header-menu'         => __( 'Header Menu' ),
			'home-aside-top-menu' => __( 'Primary home page Menu' ),
			'home-aside-menu'     => __( 'Home page Menu' ),
			'footer-menu'         => __( 'Footer Menu' )
		)
	);
}

add_action( 'init', 'register_menus' );

// Customize TinyMCE's configuration
// Force clearing text formatting on paste

function configure_tinymce($in) {
  $in['paste_preprocess'] = "function(plugin, args){
    // Strip all HTML tags except those we have whitelisted
    var whitelist = 'p,span,b,strong,i,em,h3,h4,h5,h6,ul,li,ol';
    var stripped = jQuery('<div>' + args.content + '</div>');
    var els = stripped.find('*').not(whitelist);
    for (var i = els.length - 1; i >= 0; i--) {
      var e = els[i];
      jQuery(e).replaceWith(e.innerHTML);
    }
    // Strip all class and id attributes
    stripped.find('*').removeAttr('id').removeAttr('class').removeAttr('style');
    // Return the clean HTML
    args.content = stripped.html();
  }";
  return $in;
}

add_filter('tiny_mce_before_init','configure_tinymce');
