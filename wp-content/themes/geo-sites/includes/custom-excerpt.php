<?php
/**
 * The excerpt max length
 */
function excerpt_max_length( $char_length, $symbol = '...' ) {
  $excerpt = strip_tags( get_the_excerpt() );
	if ( mb_strlen( $excerpt ) >= $char_length ) {
		$subex   = mb_substr( $excerpt, 0, $char_length - 5 );
		$exwords = explode( ' ', $subex );
		$excut   = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
      return mb_substr( $subex, 0, $excut ) . $symbol;
		} else {
      return $subex . $symbol;
		}
	} else {
		return $excerpt;
	}
}