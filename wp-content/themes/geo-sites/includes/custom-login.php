<?php
/**
 * Customize login page
 */

function custom_login_styles() {
  echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login.css" />';
}
add_action('login_head', 'custom_login_styles');

function custom_login_logo_url() {
  return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'custom_login_logo_url' );

function custom_login_logo_url_title() {
  return 'Ukraine.com - Ukraine channel';
}
add_filter( 'login_headertitle', 'custom_login_logo_url_title' );

function custom_login_message() {
  if (isset($_GET['action'])&&$_GET['action']==='register') {
    return '<p class="message">Use this form to register on ' . get_bloginfo('description') . '</p>';
  }
  return '<p class="message">Use this form to login to ' . get_bloginfo('description') . '</p>';
}
add_filter('login_message', 'custom_login_message');
