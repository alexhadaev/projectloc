<?php
// Enqueue scripts and styles.
function custom_scripts() {
	wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css', '', '2019-05-22' );
	wp_deregister_script( 'jquery' );
	wp_enqueue_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', false, false, false );
	wp_enqueue_script( 'libs', get_template_directory_uri() . '/assets/js/application.js', array( 'jquery','wp-util' ), '2019-03-07', true );
	wp_enqueue_script( 'content', get_template_directory_uri() . '/assets/js/layout/content.js', array( 'jquery' ));
	wp_enqueue_script( 'api', get_template_directory_uri() . '/assets/js/layout/ajax-requests.js', array( 'jquery','wp-api' ), '2019-03-07', true );
}
add_action( 'wp_enqueue_scripts', 'custom_scripts' );

function my_admin_enqueue_scripts() {
	wp_enqueue_script( 'my-admin-js', get_template_directory_uri() . '/assets/js/acf-api.js', array(), false, true );
}
add_action('acf/input/admin_enqueue_scripts', 'my_admin_enqueue_scripts');

/*
 * Add custom styles to wp-admin
 */
add_action( 'admin_enqueue_scripts', 'load_admin_assets' );
function load_admin_assets() {
  wp_enqueue_style( 'admin_css', get_template_directory_uri() . '/admin-styles.css', false, '2019-03-07' );
  wp_enqueue_script( 'admin_js', get_template_directory_uri() . '/assets/js/admin.js', false, '2019-03-07', true );
}

/*
 * Render assets path.
 */
function assets_path() {
	$assets_path = get_template_directory_uri() . '/assets/';
	return $assets_path;
}

require_once( ABSPATH . "wp-admin" . '/includes/image.php' );
require_once( ABSPATH . "wp-admin" . '/includes/file.php' );
require_once( ABSPATH . "wp-admin" . '/includes/media.php' );

/*
 * Add SVG to allowed file uploads.
 */
function add_file_types_to_uploads( $file_types ) {
	$new_filetypes        = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types           = array_merge( $file_types, $new_filetypes );

	return $file_types;
}

add_action( 'upload_mimes', 'add_file_types_to_uploads' );