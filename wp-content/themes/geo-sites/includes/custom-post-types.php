<?php
// Remove posts
//add_action('admin_menu', 'remove_default_post_type');

function remove_default_post_type()
{
  remove_menu_page('edit.php');
}

function register_post_types() {
  register_post_type('videos', array(
    'labels' => array(
      'name' => __('Videos'),
      'singular_name' => __('Video'),
      'edit_item' => __('Edit video'),
      'update_item' => __('Update video'),
      'add_new_item' => __('Add New video'),
      'new_item_name' => __('New videos')
    ),
    'public' => true,
    'menu_icon' => 'dashicons-format-video',
    'has_archive' => true,
    'rewrite' => array('slug' => 'videos'),
    'with_front' => true,
    'supports' => array('title', 'excerpt'),
    'hierarchical' => false
  ));

//  register_post_type('news', array(
//    'labels' => array(
//      'name' => __('News'),
//      'singular_name' => __('News'),
//      'edit_item' => __('Edit news'),
//      'update_item' => __('Update news'),
//      'add_new_item' => __('Add news'),
//      'new_item_name' => __('News')
//    ),
//    'public' => true,
//    'menu_icon' => 'dashicons-format-aside',
//    'has_archive' => true,
//    'rewrite' => array('slug' => 'news'),
//    'with_front' => false,
//    'supports' => array('title', 'excerpt', 'editor', 'thumbnail'),
//    'hierarchical' => false,
//    'menu_position' => null,
//    'cptp_permalink_structure' => '/%year%/%monthnum%/%day%/%postname%'
//  ));
//
//  register_post_type('report', array(
//    'labels' => array(
//      'name' => __('Reports'),
//      'singular_name' => __('Report'),
//      'edit_item' => __('Edit report'),
//      'update_item' => __('Update report'),
//      'add_new_item' => __('Add report'),
//      'new_item_name' => __('New report')
//    ),
//    'public' => true,
//    'menu_icon' => 'dashicons-format-status',
//    'has_archive' => true,
//    'rewrite' => array('slug' => 'report'),
//    'with_front' => false,
//    'supports' => array('title', 'excerpt', 'editor'),
//    'hierarchical' => false,
//    'menu_position' => null,
//    'cptp_permalink_structure' => '/%year%/%monthnum%/%day%/%postname%'
//  ));

  register_post_type('blog', array(
    'labels' => array(
      'name' => __('Blog'),
      'singular_name' => __('Blog'),
      'edit_item' => __('Edit post'),
      'update_item' => __('Update post'),
      'add_new_item' => __('Add post'),
      'new_item_name' => __('Blog')
    ),
    'public' => true,
    'menu_icon' => 'dashicons-admin-post',
    'has_archive' => true,
    'rewrite' => array('slug' => 'blog'),
    'with_front' => false,
    'supports' => array('title', 'excerpt', 'editor', 'thumbnail'),
    'hierarchical' => false,
    'menu_position' => null,
    'cptp_permalink_structure' => '/%postname%'
  ));

  flush_rewrite_rules();
}

add_action('init', 'register_post_types');