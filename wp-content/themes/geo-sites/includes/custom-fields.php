<?php
/**
 * Add custom fields
 */


// We connect the function of activating the meta block
function my_extra_fields() {
	add_meta_box( 'menu_title_box', 'Menu title', 'menu_title_metaBox', 'page', 'normal', 'high' );
	add_meta_box( 'params', 'Params', 'params_metaBox', 'page', 'side', 'low' );
	add_meta_box( 'city_parent', 'Select a parent city', 'location_id_metaBox', 'page', 'side', 'low' );
	add_meta_box( 'video_info', 'Video information', 'video_info_metaBox', 'videos', 'normal', 'high' );
  	add_meta_box( 'seo', 'SEO information', 'seo_metaBox', array( 'page', 'videos' ), 'normal', 'high' );
	add_meta_box( 'location_box', 'Location', 'location_metaBox', 'page', 'side', 'low' );
	add_meta_box( 'order_num', 'Page order', 'order_pages_metaBox', array( 'page' ), 'normal', 'high' );
}

add_action( 'add_meta_boxes', 'my_extra_fields' );

// Layout of a meta-box for news-feed
function order_pages_metaBox( $post ) {
  $post_id = intval( $_GET['post'] );
  ?>
  <ul>
    <li>
      <label for="#order_num" style="display:block">Order rating:</label>
      <input style="display:inline-block; width: 100%" type="number" id="news_url" name="extra[order_num]"
             value="<?php echo get_post_meta( $post_id, 'order_num', true ); ?>"/>
    </li>
  </ul>
  <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce( __FILE__ ); ?>"/>
  <?php
}

// Layout of a meta-box of params
function params_metaBox( $post ) {
	?>
  <ul>
    <li>
      <input type="hidden" name="extra[is_root_page]" value="">
      <label>
        Is it a root page?<input type="checkbox" name="extra[is_root_page]"
                                 value="1" <?php checked( get_post_meta( $post->ID, 'is_root_page', 1 ), 1 ) ?>">
      </label>
    </li>
    <li>
      <input type="hidden" name="extra[is_city]" value="">
      <label>
        Is it a city page? <input type="checkbox" name="extra[is_city]"
                                  value="1" <?php checked( get_post_meta( $post->ID, 'is_city', 1 ), 1 ) ?>
        ">
      </label>
    </li>
<!--    <li>-->
<!--      <input type="text" name="extra[old_id]" value="--><?php //echo get_post_meta( $post->ID, 'old_id', true ); ?><!--"/>-->
<!--    </li>-->
<!--    <li>-->
<!--      <input type="text" name="extra[old_url]" value="--><?php //echo get_post_meta( $post->ID, 'old_url', true ); ?><!--"/>-->
<!--    </li>-->
  </ul>
  <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce( __FILE__ ); ?>"/>
	<?php
}

// Layout of a meta-box by menu title
function menu_title_metaBox( $post ) {
	$post_id = intval( $_GET['post'] );
	?>
  <ul>
    <li>
      <label for="#menu_title"> Short title for menu: </label>
      <input style="display:inline-block; width: 50%" type="text" id="menu_title" name="extra[menu_title]"
             value="<?php echo get_post_meta( $post_id, 'menu_title', true ); ?>"/>
    </li>
  </ul>
  <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce( __FILE__ ); ?>"/>
	<?php
}

// Layout of a meta-box by SEO info
function seo_metaBox( $post ) {
	$post_id = intval( $_GET['post'] );
	?>
  <ul>
    <li>
      <label for="#seo_title" style="display:block"> Seo title: </label>
      <input style="display:inline-block; width: 100%" type="text" id="seo_title" name="extra[seo_title]"
             value="<?php echo get_post_meta( $post_id, 'seo_title', true ); ?>"/>
    </li>
    <li>
      <label for="#seo_keywords" style="display:block">Seo keywords:</label>
      <input style="display:inline-block; width: 100%" type="text" id="seo_keywords" name="extra[seo_keywords]"
             value="<?php echo get_post_meta( $post_id, 'seo_keywords', true ); ?>"/>
    </li>
    <li>
      <label for="#seo_description" style="display:block">Seo description: </label>
      <textarea style="display:block; width: 100%" name="extra[seo_description]"
                id="seo_description"> <?php echo get_post_meta( $post_id, 'seo_description', true ); ?> </textarea>
    </li>
  </ul>
  <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce( __FILE__ ); ?>"/>
	<?php
}

// Layout of a meta-box by menu title
function location_metaBox( $post ) {
	$post_id = intval( $_GET['post'] );
	?>
  <ul>
    <li>
      <label for="#latitude">Latitude: </label>
      <input style="display:inline-block; width: 100%" type="text" id="latitude" name="extra[latitude]"
             value="<?php echo get_post_meta( $post_id, 'latitude', true ); ?>"/>
    </li>
    <li>
      <label for="#longitude">Longitude: </label>
      <input style="display:inline-block; width: 100%" type="text" id="longitude" name="extra[longitude]"
             value="<?php echo get_post_meta( $post_id, 'longitude', true ); ?>"/>
    </li>
  </ul>
  <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce( __FILE__ ); ?>"/>
	<?php
}

// Layout of a meta-box by video information
function video_info_metaBox( $post ) {
	?>
  <ul>
    <li>
      <label for="#video_id">Video ID on youtube: </label>
      <input style="display:inline-block; width: 50%" type="text" id="video_id" name="extra[video_id]" required
             value="<?php echo get_post_meta( $post->ID, 'video_id', true ); ?>"/>
      <span class="spinner"></span>
      <?php $video_yourtube = "https://www.youtube.com/watch?v=" . get_post_meta( $post->ID, 'video_id', true ); ?>
      <div class="video-wrap">
        <?php echo wp_oembed_get( $video_yourtube ); ?>
      </div>
    </li>
  </ul>
  <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce( __FILE__ ); ?>"/>
	<?php
}

// Layout of a meta-box of page location id
function location_id_metaBox( $post ) {
	$post_id        = $post->ID;
	$selected_value = (int) get_post_meta( $post_id, 'location_id', 1 );

	global $wpdb;
	//Check if there is such a key in the database with the same value
	$querystr =
		"SELECT $wpdb->posts.ID
                        FROM $wpdb->posts
                        LEFT JOIN $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id
                        WHERE $wpdb->postmeta.meta_key = 'is_city'
                        AND $wpdb->postmeta.meta_value = 1        
                       ";
	$cities   = $wpdb->get_results( $querystr );

	if ( $cities ) : ?>
      <p><label for="location_id">Select page: </label>
      <select name="extra[location_id]" id="location_id">
      <option value=""></option>
		<?php foreach ( $cities as $city ) :
			$city_id = $city->ID;
			$title   = get_post_meta( $city_id, 'menu_title', 1 ) ?: get_the_title();
			if ( $selected_value == $city_id ) {
				$option = '<option value="' . $city_id . '" selected >';
				$option .= $title;
				$option .= '</option>';
			} else {
				$option = '<option value="' . $city_id . '">';
				$option .= $title;
				$option .= '</option>';
			}
			echo $option;
		endforeach;
	endif; ?>
  </select>
  </p>
  <input type="hidden" name="extra_fields_nonce" value="<?php echo wp_create_nonce( __FILE__ ); ?>"/>
	<?php
}

//By save and update custom fields
function my_extra_fields_update( $post_id ) {
	if ( ! wp_verify_nonce( $_POST['extra_fields_nonce'], __FILE__ ) ) {
		return false;
	}
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return false;
	} // Exit if this autosave
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return false;
	} // Exit if the user does not have the right to edit the record

	if ( ! isset( $_POST['extra'] ) ) {
		return false;
	} // If there is no data? left


	$_POST['extra'] = array_map( 'trim', $_POST['extra'] ); // Clean all data from spaces at the edges
	foreach ( $_POST['extra'] as $key => $value ) {
		if ( empty( $value ) ) {
			delete_post_meta( $post_id, $key ); // Delete the field if the value is empty
			continue;
		}

		if ( ! ( strpos( $key, 'order_by_' ) === false ) ) { //If saving the field with "order_by_"
			global $wpdb;

			//Check if there is such a key in the database with the same value
			$querystr =
				"SELECT $wpdb->posts.*
                        FROM $wpdb->posts
                        LEFT JOIN $wpdb->postmeta ON $wpdb->posts.ID = $wpdb->postmeta.post_id
                        WHERE $wpdb->postmeta.meta_key = %s
                        AND $wpdb->postmeta.meta_value = %d        
                       ";
			$rolls    = $wpdb->get_results( $wpdb->prepare( $querystr, $key, $value ) );
			//If there is such a value, we delete it
			if ( $rolls ) {
				foreach ( $rolls as $post ) {
					delete_post_meta( $post->ID, $key );
				}
			}
		}
		update_post_meta( $post_id, $key, $value );
	}

	return $post_id;
}

add_action( 'save_post', 'my_extra_fields_update', 0 );
