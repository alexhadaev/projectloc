<?php
/*
 * Fetch video data from Youtube by ID
 *****************************************************************************************/

function fetch_video_data() {
	if ( isset( $_REQUEST ) && $_REQUEST['video_id'] != '') {
    $video_id               = $_REQUEST['video_id'];
    // create the context
    $arContext['http']['timeout'] = 3;
    $context = stream_context_create($arContext);

    // Fetch data
    $jsonURL = file_get_contents("https://www.googleapis.com/youtube/v3/videos?id=" . $video_id . "&key=AIzaSyChoIAQk9lI03gwfqw2b65eqOjKJ9116go&part=snippet", 0, $context);
    $json = json_decode($jsonURL);

    $video = (object)[];
    $video->title = $json->{'items'}[0]->{'snippet'}->{'title'};
    $video->description = $json->{'items'}[0]->{'snippet'}->{'description'};
    $video->tags = $json->{'items'}[0]->{'snippet'}->{'tags'};
    if (is_array ($video->tags)) {
      $video->tags = implode(",",$video->tags);
    }
    $response = json_encode($video);

    echo $response;
  }
	die();
}

add_action( 'wp_ajax_fetch_video_data', 'fetch_video_data' );
add_action( 'wp_ajax_nopriv_fetch_video_data', 'fetch_video_data' );