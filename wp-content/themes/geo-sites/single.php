<?php
/**
 * The template for displaying all single posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package geo-sites
 */
acf_form_head();
get_header();
//echo '<br>'. get_term_parents_list( 1, 'category', array(
//		'separator' => ' / ',
//		'inclusive' => true
//	) );

//
//$taxonomy_names = get_object_taxonomies( 'post' );
//var_dump( $taxonomy_names);
//wp_tag_cloud( 'taxonomy=category&smallest=15&largest=40&number=50&orderby=count' );
//$published_posts = wp_count_posts()->publish;
//var_dump($published_posts);
//
//wp_page_menu();
?>
  <div class="single-post container" id="post">
      <?= do_shortcode("[baztag]");?>
       <br>
	  <?php get_template_part( 'layouts/youtube-data' ); ?>

      <div class="row">
<!--          --><?php //var_dump(get_field('group',86));?>
<!--          <br>=========================-->
<!--          --><?php //var_dump(get_metadata('post', 86, 'group'));?>

      <?php while (have_posts()) : the_post(); ?>
        <main class="main">
            <hr>
            <div id="popup-id" data-id="<?php the_ID() ?>">
	            <?php acf_form(array(
		            'id'           => get_the_ID(),
		            'post_id'      => false,
		            'post_title'   => true,
		            'submit_value'	=> 'Обновить пост!'
	            )); ?>
            </div>

            <hr>
          <?php get_template_part('layouts/content-post'); ?>
            <?php
          // If comments are open or we have at least one comment, load up the comment template.
          if ( comments_open() || get_comments_number() ) :
	          comments_template();
          endif;
          ?>
        </main>
        <aside class="aside">
          <?php get_template_part( 'layouts/booking-widget' ); ?>
          <?php get_template_part( 'layouts/advertisement-aside' ); ?>
        </aside>

      <?php endwhile; // end of the loop. ?>
    </div>


      <ul class="comments_wrap">
      </ul>
  </div>

<?php
acf_enqueue_uploader();
?>
    <script type="text/javascript">
        (function($) {
            // добавляем поля
            acf.do_action('append', $('#popup-id'));

        })(jQuery);
    </script>
<?php
get_footer();
