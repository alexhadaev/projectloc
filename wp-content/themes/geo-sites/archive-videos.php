<?php
/**
 * The template for displaying all videos
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package geo-sites
 */

get_header(); ?>

  <div class="wrapper videos-page">
    <div class="container">
      <div class="row">
        <main class="main">
          <h2 class="section-title"><span>Videos</span></h2>
          <?php
          $currentPage = get_query_var( 'paged' );
          $args           = array(
            'post_type'      => 'videos',
            'post_status'    => 'publish',
            'posts_per_page' => get_option( 'posts_per_page' ),
            'paged'          => $currentPage,
          );
          $videos         = new WP_Query( $args ); ?>
          <?php if ( $videos->have_posts() ) : ?>
            <ul class="row video-list ">
              <?php while ( $videos->have_posts() ) : $videos->the_post();
                $video_yourtube_id = get_post_meta( $post->ID, 'video_id', true );
                $img_url           = 'https://i3.ytimg.com/vi/' . $video_yourtube_id . '/hqdefault.jpg';
                $title             = get_the_title();
                ?>
                <li class="col-sm-6 video-block item">
                  <a class="youtube-image-container" href="<?php the_permalink( $post->ID ); ?>">
                    <img src="<?= $img_url ?>" alt="<?= $title ?>">
                  </a>
                  <h3 class="item-title">
                    <a href="<?php the_permalink( $post->ID ); ?>"><?= $title ?></a>
                  </h3>
                </li>
              <?php endwhile; ?>
            </ul>
            <div class="pagination">
              <?php echo paginate_links( array(
                  'total'     => $videos->max_num_pages,
                  'prev_text' => '<span> < </span><span>' . __( 'Prev', '' ) . '</span>',
                  'next_text' => '<span>' . __( 'Next', '' ) . '</span><span> > </span>',
                  'mid_size'  => 5,
                )
              ); ?>
            </div>
            <?php wp_reset_postdata(); ?>
          <?php endif; ?>
        </main>
        <aside class="aside">
          <?php get_template_part('layouts/aside-podcasts'); ?>
          <?php get_template_part( 'layouts/booking-widget' ); ?>
          <?php get_template_part( 'layouts/advertisement-aside' ); ?>
        </aside>
      </div>
    </div>
  </div>

<?php get_footer();