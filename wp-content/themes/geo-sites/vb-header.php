<?php
/*
 * Load wp-header for vBulletin
 *****************************************************************************************/

function load_header() { ?>
    <style><?php include 'vb-header.css'; ?></style>
    <div class="wp-header">
        <div class="vb-hero" style="background-image: url('<?= get_header_image() ?>')">
            <a href="<?php echo get_home_url(); ?>" class="hero-logo"><img src="<?= assets_path(); ?>/img/logo.svg" alt="<?php _e('Ecuador.com logo') ?>"></a>
        </div>
        <nav>
            <div class="vb-container">
                <span role="button" class="btn-menu icon-menu"></span>
                <!-- The WordPress Menu goes here -->
                <?php wp_nav_menu(array(
                    'theme_location' => 'header-menu',
                    'menu_class' => 'navbar-nav',
                    'fallback_cb' => '',
                ));?>
            </div>
        </nav>
    </div>
    <?php die();
}

add_action( 'wp_ajax_load_header', 'load_header' );
add_action( 'wp_ajax_nopriv_load_header', 'load_header' );