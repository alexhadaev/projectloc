var gulp         = require('gulp'),
    sass         = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleancss     = require('gulp-clean-css'),
    concat       = require('gulp-concat'),
    uglify       = require('gulp-uglify');


gulp.task('styles', function () {
    return gulp.src('./assets/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(['last 2 versions']))
        .pipe(cleancss())
        .pipe(gulp.dest(''));
});

gulp.task('scripts', function() {
    return gulp.src([
        './assets/js/vendors/**/*.js',
        './assets/js/layout/**/*.js'
    ])
        .pipe(concat('application.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./assets/js/'));
});

gulp.task('watch', function () {
    gulp.watch('assets/scss/**/*.scss', ['styles']);
    gulp.watch('assets/js/vendors/*.js', ['scripts']);
	gulp.watch('assets/js/layout/*.js', ['scripts']);
});

gulp.task('default', ['styles', 'scripts', 'watch']);