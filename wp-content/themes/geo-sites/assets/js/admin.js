jQuery('document').ready(function ($) {
  /**
   * Fetch video data
   */
  $('#video_id').on('focusout', function () {
    var video_id = $(this).val(),
        $spinner = $('#video_info .spinner');
    // This does the ajax request to load posts
    $.ajax({
      url: ajaxurl,
      data: {
        'action': 'fetch_video_data',
        'video_id': video_id
      },
      beforeSend: function () {
        $spinner.css('visibility', 'visible');
      },
      success: function (data) {
        $spinner.css('visibility', 'hidden');
        console.log(data);

        var response = jQuery.parseJSON(data);
        $('#title').val(response.title).focus();
        $('#excerpt').html(response.description);
        $('#seo_title').val(response.title);
        $('#seo_keywords').val(response.tags);
        $('#seo_description').html($.trim(response.description));
        $('.video-wrap iframe').remove();
        $('<iframe width="500" height="375" frameborder="0" allowfullscreen></iframe>')
          .attr("src", "https://www.youtube.com/embed/" + video_id + "?feature=oembed")
          .appendTo(".video-wrap");
      },
      error: function (errorThrown) {
        $spinner.css('visibility', 'hidden');
        console.log(errorThrown);
      }
    });
  });
});