$(document).on('ready', function () {

  function updateThumbnailImage(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('.add-article .thumbnail img').attr('src', e.target.result);
      };

      reader.readAsDataURL(input.files[0]);
    }
  }

  function articleValidation() {
    var validation = true;
    var form = $('#add_article_from');
    var title = form.find('#article_title').val();
    var excerpt = form.find('#article_excerpt').val();
    var content = form.find('#article_content').val();

    if (title === '') {
      $('#article_title').addClass('error');
      validation = false;
    } else {
      $('#article_title').removeClass('error');
    }

    if (excerpt === '') {
      $('#article_excerpt').addClass('error');
      validation = false;
    } else {
      $('#article_excerpt').removeClass('error');
    }

    if (content === '') {
      $('#wp-article_content-wrap').addClass('error');
      validation = false;
    } else {
      $('#wp-article_content-wrap').removeClass('error');
    }

    return validation;
  }

  $('.add-thumbnail-button').on('click', function () {
    $('#article_thumbnail').trigger('click');
  });

  $('#article_thumbnail').bind('change', function () {
    var validate = true;
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
      validate = false;
    }
    var fileSize = this.files[0].size;
    if (fileSize > 1048576) {
      validate = false;
    }
    if (!validate) {
      $(this).val('');
      $('.thumbnail-requirements').addClass('error');
      $('.thumbnail img').attr('src', assetsurl + "/img/no-image.jpg)");
    } else {
      updateThumbnailImage(this);
      $('.thumbnail-requirements').removeClass('error');
    }
  });

  $('#accept_terms').change(function () {
    var $submit = $('#add_article_from .submit');
    if ($submit[0].hasAttribute('disabled')) {
      $submit.removeAttr('disabled');
    } else {
      $submit.attr('disabled', 'disabled');
    }
  });

  $(document).on('click', '#add_article_from .submit', function (e) {
    e.preventDefault();
    if (articleValidation() && $('#accept_terms').is(':checked')) {
      $('.spinner-wrapper').fadeIn();
      var form = $('#add_article_from');

      var title = $('#article_title').val(),
          excerpt = $('#article_excerpt').val(),
          content = $('#article_content').val(),
          author = $('#article_author').val();

      var data = new FormData();
      data.append('action', 'send_article');
      data.append('title', title);
      data.append('content', content);
      data.append('excerpt', excerpt);
      data.append('author', author);

      jQuery.each(jQuery('#article_thumbnail')[0].files, function (i, file) {
        data.append('thumbnail', file);
      });

      $.ajax({
        url: ajaxurl,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function (data) {
          var validation = data.toString();
          $('.spinner-wrapper').fadeOut();
          $('#add_article_from').fadeOut();
          if (validation == 'true') {
            console.log('1');
            $('.article-send-success').fadeIn();
          } else {
            console.log('0');
            $('.article-send-error').fadeIn();
          }
        },
        error: function (errorThrown) {
          $('.spinner-wrapper').fadeOut();
          console.log(errorThrown);
        }
      });
    }
  });
});