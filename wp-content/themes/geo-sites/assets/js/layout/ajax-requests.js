jQuery('document').ready(function ($) {
    /**
     * Load more posts via ajax
     */
    $('.latest-posts .load-more').on('click', function () {
        var page = $(this).attr('data-page');
        // This does the ajax request to load posts
        $.ajax({
            url: ajaxurl,
            data: {
                'action': 'load_posts',
                'page': page
            },
            beforeSend: function(){
                $('.latest-posts .next-page').remove();
                $('.spinner-wrapper').fadeIn();
            },
            success: function (data) {
                $('.spinner-wrapper').fadeOut();
                $('.latest-posts .row').append(data);
                var nextPage = $('.latest-posts .next-page').attr('data-page').toString();
                if (nextPage !== '') {
                    $('.latest-posts .load-more').attr('data-page', nextPage);
                } else {
                    $('.latest-posts .load-more').remove();
                }
            },
            error: function (errorThrown) {
                $('.spinner-wrapper').fadeOut();
                console.log(errorThrown);
            }
        });
    });

    /**
     * Load more videos via ajax
     */
    $('.random-videos .load-more').on('click', function () {
        var page = $(this).attr('data-page'),
            exclude_videos_ids = [];
        $('.random-videos .item').each( function() {
            exclude_videos_ids.push($(this).attr('data-id'));
        });

        // This does the ajax request to load videos
        $.ajax({
            url: ajaxurl,
            data: {
                'action': 'load_videos',
                'page': page,
                'exclude_videos_ids': exclude_videos_ids
            },
            beforeSend: function(){
                $('.random-videos .next-page').remove();
                $('.spinner-wrapper').fadeIn();
            },
            success: function (data) {
                $('.spinner-wrapper').fadeOut();
                $('.random-videos .row').append(data);
                var nextPage = $('.random-videos .next-page').attr('data-page').toString();
                if (nextPage !== '') {
                    $('.random-videos .load-more').attr('data-page', nextPage);
                } else {
                    $('.random-videos .load-more').remove();
                }
            },
            error: function (errorThrown) {
                $('.spinner-wrapper').fadeOut();
                console.log(errorThrown);
            }
        });
    });

    //******************************************
    $.ajax( {
        url: wpApiSettings.root + 'wp/v2/posts?page=1&order=asc',
        method: 'GET',

        beforeSend: function ( xhr ) {
            xhr.setRequestHeader( 'X-WP-Nonce', wpApiSettings.nonce );
        },

    } ).done( function ( response ) {
        console.log('response = ', response);
        $.each(response, function(i, el){
            $('#post').append(
                el.content.rendered
            );

            // $.ajax( {
            //     url: el._links["wp:featuredmedia"][0].href,
            //     method: 'GET',
            //     beforeSend: function ( xhr ) {
            //         xhr.setRequestHeader( 'X-WP-Nonce', wpApiSettings.nonce );
            //     }
            // } ).done( function ( response ) {
            //     console.log(response);
            //     $('#post').append(
            //         response.description.rendered
            //     );
            // });
            // console.log(el);
        })

    } );
    // alert();

    //******************* шаблонизатор wp

    var
        singleTemplate = wp.template( 'comment-single' ), // наш шаблон добавить зависимость скрипта 'wp-util'
        $comments      = $('.comments_wrap'),              // контейнер с комментариями
        $commentForm   = $('#respond');                 // форма комментирования

// функция обработки AJAX ответа

    $.ajax( {
        url: wpApiSettings.root + 'wp/v2/comments',
        method: 'GET',
        data: {
            post: '1'
        },
        beforeSend: function ( xhr ) {
            xhr.setRequestHeader( 'X-WP-Nonce', wpApiSettings.nonce );
        },

    } ).done( function ( data ) {
        $.each(data, function (i, el) {
            commentSuccess( el );
            // console.log(el);

        });

    } );

    var $data = {
        gravatar: 'gravatar',
        comment_author_url: 'comment_author_url',
        comment_author: 'comment_author',
        comment_class: 'comment_class',
        comment_ID: 'comment_ID'
    };
    // commentSuccess( $data );
    function commentSuccess( data ){
        // создаем HTML по шаблону
        $comments.append( singleTemplate( data ) );
        // console.log(data);
        // сбрасываем форму
        // $commentForm.get(0).reset();
    }

    //acf ajax update post
    // $('.acf-button').on('click', function (e) {
    //     e.preventDefault();
    //     var id = $('#popup-id').data('id');
    //     console.log(id);
    //     $.ajax({
    //         url: ajaxurl,
    //         data: {
    //             'action': 'update_post',
    //             'id': id
    //         },
    //         success: function (data) {
    //             $('#popup-id').append(data);
    //         }
    //     });
    // });


});