function setCookie(cookieName, cookieValue, exDays) {
  var d = new Date();
  d.setTime(d.getTime() + (exDays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = cookieName + "=" + cookieValue + ";" + expires + ";path=/";
}

function getCookie(cookieName) {
  var name = cookieName + '=';
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
}

jQuery('document').ready(function ($) {

  var $cookiePopup = $('.cookie-popup'),
    cookieName = window.location.hostname + '_accepting_terms';

  function checkAcceptingTerms() {
    var cookieValue = getCookie(cookieName);
    if (cookieValue !== '') {
      $cookiePopup.hide();
    } else {
      $cookiePopup.fadeIn();
    }
  }

  checkAcceptingTerms();

  $('.cookie-popup .accept').on('click', function () {
    setCookie(cookieName, 'true', 365);
    $cookiePopup.fadeOut();
  });

  var $supportPopup = $('.support-popup'),
    supportName = window.location.hostname + '_accepting_support';

  function checkAcceptingSupport() {
    var supportValue = getCookie(supportName);

    if (adBlockDetect || flashBlockDetect) {
      if (supportValue !== '') {
        $supportPopup.hide();
      } else {
        $supportPopup.fadeIn();
      }
    }
    else
    {
      $supportPopup.hide();
    }
  }

  $('.support-popup .btn').on('click', function () {
    setCookie(supportName, 'true', 1);
    $supportPopup.fadeOut();
  });

  checkAcceptingSupport();

  var adBlockDetect = function () {
    var adBlockEnabled = false,
      testAd = document.createElement('div');

    testAd.innerHTML = '&nbsp;';
    testAd.className = 'adsbox';
    document.body.appendChild(testAd);

    if (testAd.offsetHeight === 0) {
      adBlockEnabled = true;
    }
    testAd.remove();

    return adBlockEnabled;
  };

  var flashBlockDetect = function (callbackMethod) {
    var return_value = 0;

    if (navigator.plugins["Shockwave Flash"]) {
      embed_length = $('embed').length;
      object_length = $('object').length;

      if ((embed_length > 0) || (object_length > 0)) {
        /* Mac / Chrome using FlashBlock + Mac / Safari using AdBlock */
        $('object, embed').each(function () {
          if ($(this).css('display') === 'none') {
            return_value = 2;
          }
        });
      } else {
        /* Mac / Firefox using FlashBlock */
        if ($('div[bginactive]').length > 0) {
          return_value = 2;
        }
      }
    } else if (navigator.userAgent.indexOf('MSIE') > -1) {
      try {
        new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
      } catch (e) {
        return_value = 2;
      }
    } else {
      /* If flash is not installed */
      return_value = 1;
    }

    if (callbackMethod && typeof(callbackMethod) === "function") {
      callbackMethod(return_value);
    } else {
      return return_value;
    }
  }
});