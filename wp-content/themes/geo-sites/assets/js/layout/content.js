$(function () {
  $("img, a").on("dragstart", function (event) {
    event.preventDefault();
  });
});


jQuery('document').ready(function ($) {
  $('#remove_script').click(function () {
    console.log('hello');
    // $.getScript( "wp-content/themes/geo-sites/assets/js/layout/test.js", function( data, textStatus, jqxhr ) {
    //   console.log( data ); // Data returned
    //   console.log( textStatus ); // Success
    //   console.log( jqxhr.status ); // 200
    //   console.log( "Load was performed." );
    //   add(2,3);
    // });

    let script = document.createElement('script');
    script.src = "wp-content/themes/geo-sites/assets/js/layout/test.js";
    document.head.append(script);

    script.onload = function() {
      add(2,3);
    };
  });

  /**
   * Initiate masonry effect on pages section
   */
  $('.pages-list').masonry({
    itemSelector: '.item'
  });

  /**
   * Initiate parallax effect on hero section
   */
  $('.parallax').parallax();

  /**
   * Toggle mobile menu
   */
  var $mainMenu = $('.menu-header-menu-container');

  $('.btn-menu').click(function () {
    $mainMenu.slideToggle(300);
    $(this).toggleClass('icon-close');
  });

  $(window).on('resize', function () {
    fixMenu();
  });

  function fixMenu() {
    if ($('body').innerWidth() > 992) {
      $mainMenu.show();
    } else {
      $mainMenu.hide();
    }
    $('.btn-menu').removeClass('icon-close');
  }

  /**
   * Validate search form
   */
  $('.search-form').on('submit', function (e) {
    var searchQuery = $(this).find('input').val();
    if (searchQuery != '') {
      $(this).submit();
    } else {
      e.preventDefault();
    }
  });


  //add title from youtube
  var id = 'eRFgIh-raT8';
  var url = 'https://www.youtube.com/watch?v=' + id;

  $.getJSON('https://noembed.com/embed',
      {format: 'json', url: url}, function (data) {
        console.log(data.title);
        // console.log(data);
      });




});