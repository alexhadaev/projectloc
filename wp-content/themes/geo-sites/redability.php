<?php
require_once 'simple_html_dom.php';
$site_url = $_GET['site_url'];
$site_text = $_GET['site_text'];
$readability_text = '';

if ($site_url != ''){
	require_once 'simple_html_dom.php';
	$html = file_get_html($_GET['site_url']);
	echo '1111111111111111111111111111111111111111111111111111111111111111';
	//echo $html;
	$body = $html->find('body', 0);
	$tags = $body->find('p,span,h1,h2,h3,h4,h5,li');
	$i = 0;


	foreach($tags as $text){
//	echo $text . ' ' .' = '. substr(trim(strip_tags($text)), -1); // returns "…";
//	echo "<br>";
		$tag = $text->tag;

		$punctuation = substr(trim(strip_tags($text)), -1);
//	var_dump('$punctuation = ', $punctuation, $tag);
        if ($tag == 'li'){
            if ($punctuation != '.' && $punctuation != '!' && $punctuation != '?' && $punctuation != ';' ){
                $text->outertext = '';
    //			echo '(=== '. trim($text) .'====)<br>';
            }
    //		echo '(=== '. trim($text) .'====)<br>';
        }
        if ($text != ''){
	        if ($punctuation != '.' && $punctuation != '!' && $punctuation != '?' && $punctuation != ';' && $punctuation != ':' && $punctuation != '*'){
		        $text = trim(strip_tags($text)) . '. ';
//			echo '(=== '. trim($text) .'====)';
	        }
        }

		$readability_text = $readability_text . ' ' . trim(strip_tags($text));
		echo $text;
	}

	$html->clear();
	unset($html);
}elseif ($site_text != ''){
	echo '222222222222222222222222222222222222222222222222222222222222222222';
	$readability_text = $site_text;
}


//$readability_text = 'Strategy Design Development Strategy Design Development First-rate medical care deserves a first-rate digital experience... Creating effortless digital experiences for healthcare payers, providers and their customers...';
var_dump($readability_text);

//character Count ***************************
$characterCount = strlen(preg_replace('/\W/i', '', $readability_text));
var_dump('$characterCount = ', $characterCount, preg_replace('/\W/i', '', $readability_text));

//count world ********************************
$strip_word = str_replace(array("'s", "’s", "'t", "’t"), '', $readability_text);
$wordCount = str_word_count($strip_word, 0);//, '01234567893'
var_dump('$wordCount = ', $wordCount);

//count Sentences ****************************
var_dump('count($tags) = ', count($tags));
function countSentences($x){
	$find = array('Mr.', 'Mrs.', 'Ms.', 'i.e.', 'e.g.', 'vs.', '..');
	$x = str_replace($find,'',$x);
	return preg_match_all('/[^\s](\.|\!|\?)(?!\w)/', $x, $match);
}
$sentenceCount = countSentences($readability_text);
var_dump('$sentenceCount = ',$sentenceCount);

//syllable Count *****************************
$syllableCount = countSyllables($readability_text);
var_dump('$syllableCount = ', $syllableCount);
function countSyllables($x) {
	$triples = 'dn\вЂ™t|dn\'t|eau|iou|ouy|you|bl\s';
	$doubles = 'ai|ae|ay|au|ea|ee|ei|eu|ey|ie|ii|io|oa|oe|oi|oo|ou|oy|ue|uy|ya|ye|yi|yo|yu';
	$singles = 'a|e|i|o|u|y';

	// Cleaning up symbols
	$find_symbol = array('!','?','.','.',',',':');
	$x = str_replace($find_symbol, '', $x);

	//add last whitespace
	$x = $x . ' ';

	// Cleaning up word endings
	$find_letter = array('s ','e ');
	$x = str_replace($find_letter, ' ', $x);
//	var_dump('replace end w = ',$x);
//		var_dump(preg_match_all('/(' . $triples . '|' . $doubles . '|' . $singles . ')/i', $x, $match));
	return preg_match_all('/(' . $triples . '|' . $doubles . '|' . $singles . ')/i', $x, $match);
}


// polysyllables (complex words) *******************************
$polysyllables = polysyllables($readability_text);
var_dump('$polysyllables = ',$polysyllables);
function polysyllables($x){
	$array_polysyllables = explode(' ', $x);
	$count_polysyllables = 0;
	foreach($array_polysyllables as $word) {
		$word = $word . ' ';
		if (countSyllables($word) >= 3) {
//			echo $word . ' ';
			$count_polysyllables++;
		}
	}
	return $count_polysyllables;
}

//Percent of complex words
$percentComplexWords = ($polysyllables * 100)/$wordCount;
var_dump('Percent of complex words = ', $percentComplexWords);

//Average words per sentence
$averageSentenceLength = $wordCount / $sentenceCount;
var_dump('Average words per sentence = ', $averageSentenceLength);

//Average syllables per word
$averageSyllablesWord = $syllableCount / $wordCount;
var_dump('Average syllables per word = ', $averageSyllablesWord);

/****/
require_once 'readability-formulae.php';

//Flesch Kincaid Reading Ease: 206.835 - 1.015 x (words/sentences) - 84.6 x (syllables/words);
getFleschScore($wordCount, $sentenceCount, $syllableCount);
var_dump('Flesch Kincaid Reading Ease = ',getFleschScore($wordCount, $sentenceCount, $syllableCount));

//Flesch Kincaid Grade Level: 0.39 x (words/sentences) + 11.8 x (syllables/words) - 15.59;
getFleschKincaid($wordCount, $sentenceCount, $syllableCount);
var_dump('Flesch Kincaid Grade Level = ', getFleschKincaid($wordCount, $sentenceCount, $syllableCount));

//Gunning Fog Score:  0.4 x ( (words/sentences) + 100 x (complexWords/words) );
getGunningIndex($sentenceCount, $polysyllables, $wordCount);
var_dump('Gunning Fog Score = ',getGunningIndex($sentenceCount, $polysyllables, $wordCount));

//SMOG Index: 1.0430 x sqrt( 30 x complexWords/sentences ) + 3.1291;
var_dump('SMOG Index = ',getSmog($polysyllables, $sentenceCount));

//Coleman Liau Index: 5.89 x (characters/words) - 0.3 x (sentences/words) - 15.8;
var_dump('Coleman Liau Index = ', getColemanLiau($characterCount, $sentenceCount, $wordCount));

//Automated Readability Index (ARI): 4.71 x (characters/words) + 0.5 x (words/sentences) - 21.43;
var_dump('Automated Readability Index (ARI) = ', getAri($characterCount, $wordCount, $sentenceCount));

//output
?>
<h2>READABILITY INDICES</h2>
<h3>Flesch Kincaid Reading Ease</h3>
<span><?php echo round(getFleschScore($wordCount, $sentenceCount, $syllableCount)['score'], 1); ?></span><br>
<span> <?php echo round(getFleschScore($wordCount, $sentenceCount, $syllableCount)['grade'], 1); ?></span>
<div class="indicator" style="background-color: red; width: 30%"></div>
<h3>Flesch Kincaid Grade Level</h3>
<span><?php echo round(getFleschKincaid($wordCount, $sentenceCount, $syllableCount), 1); ?></span>
<h3>Gunning Fog Score</h3>
<span><?php echo round(getGunningIndex($sentenceCount, $polysyllables, $wordCount), 1); ?></span>
<h3>SMOG Index</h3>
<span><?php echo round(getSmog($polysyllables, $sentenceCount), 1); ?></span>
<h3>Coleman Liau Index</h3>
<span><?php echo round(getColemanLiau($characterCount, $sentenceCount, $wordCount), 1); ?></span>
<h3>Automated Readability Index</h3>
<span><?php echo round(getAri($characterCount, $wordCount, $sentenceCount), 1); ?></span>
<h2>TEXT STATISTICS</h2>
<h3>No. of sentences</h3>
<span><?php echo $sentenceCount; ?></span>
<h3>No. of words</h3>
<span><?php echo $wordCount; ?></span>
<h3>No. of complex words</h3>
<span><?php echo $polysyllables; ?></span>
<h3>Percent of complex words</h3>
<span><?php echo round($percentComplexWords, 2); ?>%</span>
<h3>Average words per sentence</h3>
<span><?php echo round($averageSentenceLength, 2); ?></span>
<h3>Average syllables per word</h3>
<span><?php echo round($averageSyllablesWord, 2); ?></span>