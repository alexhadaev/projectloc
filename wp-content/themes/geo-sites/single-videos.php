<?php
/**
 * The template for displaying all single video
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package geo-sites
 */

get_header(); ?>

  <div class="single-post container">
    <div class="row">
      <?php while (have_posts()) : the_post(); ?>
        <main class="main">
          <?php get_template_part('layouts/content-video'); ?>
        </main>
        <aside class="aside">
          <?php get_template_part( 'layouts/booking-widget' ); ?>
          <?php get_template_part( 'layouts/advertisement-aside' ); ?>
        </aside>
      <?php endwhile; // end of the loop. ?>
    </div>
  </div>

<?php get_footer();
