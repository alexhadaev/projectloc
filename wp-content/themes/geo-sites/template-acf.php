<?php
/*
 * Template name: Flexible Content
 *
 */
get_header();
?>
<div class="container">
    <h2>Flexible</h2>
    <hr>
	<?php

	// проверяем есть ли данные в гибком содержании
	if( have_rows('flexible_image') ):

		// перебираем данные
		while ( have_rows('flexible_image') ) : the_row();

			if( get_row_layout() == 'block' ):?>

                <h2><?php the_sub_field('title'); ?></h2>
                <img src="<?php the_sub_field('image'); ?>" alt="">
				<p><?php the_sub_field('description'); ?></p>

            <?php elseif( get_row_layout() == 'flexible_caption' ): ?>

                <h3><b><?php the_sub_field('name'); ?></b></h3>
                <p><i><?php the_sub_field('caption'); ?></i></p>

			<?php endif;

		endwhile;
	endif;
	?>
    <hr>
    <h2>Group</h2>
    <hr>
	<?php

	// переменные
	$hero = get_field('group');

	if( $hero ): ?>
        <div id="hero">
            <img src="<?php echo $hero['image_group']; ?>" alt="<?php echo $hero['title_group']; ?>" />
            <div class="content">
				<h2><?php echo $hero['title_group']; ?></h2>
				<p><?php echo $hero['caption_group']; ?></p>

            </div>
        </div>
	<?php endif; ?>
</div>
<div class="container">
	<?php
	// получим HTML iframe
	$iframe = get_field('embed');
	// используем preg_match чтобы найти iframe src
	preg_match('/src="(.+?)"/', $iframe, $matches);
	$src = $matches[1];
	// добавим дополнительные параметры к src
	$params = array(
		'controls'    => 1,
		'hd'        => 1,
		'autohide'    => 1
	);
	$new_src = add_query_arg($params, $src);
	$iframe = str_replace($src, $new_src, $iframe);
	// добавим дополнительные параметры к html iframe
	$attributes = 'frameborder="0"';
	$iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
	// Выведем $iframe
	echo $iframe;
	?>

    <hr>
    <h2>Post object</h2>
    <hr>
    <?php
//    var_dump(get_field('post_object'));

    $post_objects = get_field('post_object');

    if( $post_objects ): ?>
        <ul>
            <?php foreach( $post_objects as $post): // Переменная должна быть названа обязательно $post (IMPORTANT) ?>
                <?php
                    setup_postdata($post);
//                    var_dump($post);
                    echo $post->post_content;
                ?>
                <li>
                    <a href="<?= $post->guid; ?>"><?= $post->post_title; ?></a>
                    <span>Объект записи произвольного поля: <?php echo $post->post_name; ?></span>
                </li>
            <?php endforeach; ?>
        </ul>
	<?php wp_reset_postdata(); // ВАЖНО - сбросьте значение $post object чтобы избежать ошибок в дальнейшем коде ?>
	<?php endif; ?>
    <hr>
    <h2>Page link</h2>
    <hr>
	<?php
	// переменные
	$post_id = get_field('page_link', false, false);
	// проверяем на существование поля
	if( $post_id ): ?>
        <a href="<?php echo get_the_permalink($post_id); ?>"><?php echo get_the_title($post_id); ?></a><br>
        <a href="<?php echo get_the_permalink($post_id[1]); ?>"><?php echo get_the_title($post_id[1]); ?></a>
	<?php endif; ?>
    <hr>
	<?php
	// переменные
	$urls = get_field('page_link');
	// проверяем на существование поля
	if( $urls ): ?>
        <ul>
			<?php foreach( $urls as $url ): ?>
                <li>
                    <a href="<?php echo $url ?>"><?php echo $url; ?></a>
                </li>
			<?php endforeach; ?>
        </ul>
	<?php endif; ?>
</div>
<?php get_footer(); ?>