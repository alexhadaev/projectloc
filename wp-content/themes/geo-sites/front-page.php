<?php
/**
 * Template Name: Home Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package geo-sites
 */

get_header(); ?>

  <div class="home-page container">
      <button id="remove_script">===ok===</button>
      <div id="add-iframe"></div>
      <br>
      <button><?php _e('send', 'mytranslate'); ?></button>

	  <?php if ( function_exists( 'dimox_breadcrumbs' ) ) dimox_breadcrumbs(); ?>
      <div class="row">
      <main class="main">

          <?php
//          $post_types = get_post_types([],'objects');
//
//          foreach( $post_types as $post_type => $value ) {
//	          echo $post_type ."<br>" .$value->label ;
////	          var_dump($value->labels->add_new);
//          }

          $query = new WP_Query(array(
	          'post_type'=>'nav_menu_item',
//	          'orderby' => 'title',
//	          'order' => 'ASC',
          ));
//var_dump($query);

//          $childrens = get_children( [
////	          'post_parent'    => 733,
//	          'post_parent' => null,
//	          'post_type'   => 'nav_menu_item',
//	          'numberposts' => -1,
//	          'post_status' => 'any'
//          ] );
//
//          if( $childrens ){
//	          foreach( $childrens as $children ){
////		          var_dump($children->ID);
//		           get_ancestors( $children->ID, '', 'nav_menu_item' );
//	          }
//          }
//          the_meta();
//          the_date();
//          edit_post_link();


          $text = "<p><b> Это \"Makes Sense\" & 'Имеет смысл'!</b></p>";
          echo esc_html( $text );

          /* Получим:
		  &lt;p&gt;&lt;b&gt; Это &quot;Makes Sense&quot; &amp; &#039;Имеет смысл&#039;!&lt;/b&gt;&lt;/p&gt;
		  */

          $array = wp_get_attachment_metadata( 11 );

              var_dump($array['sizes']['thumbnail']['file']);
              var_dump($array['sizes']['medium']['file']);
//              var_dump($array);
              echo '<img src="https://project.dev/wp-content/uploads/2019/07/'. $array['sizes']['thumbnail']['file'] .'">';
              echo '<img src="https://project.dev/wp-content/uploads/2019/07/'. $array['sizes']['medium']['file'] .'">';

          $is_image = file_is_displayable_image( 'https://project.dev/wp-content/uploads/2019/07/apple-touch-icon-180x180.png' ); //> false

          if ( $is_image ) {
	          echo 'Файл является изображением';
          }
          else {
	          echo 'Файл не является изображением <br>';
          }


          $url = 'https://example.com/path?arg=value#anchor';

          $parts = wp_parse_url( $url );


          echo wp_parse_url( $url, PHP_URL_SCHEME ).'<br>';   // https
          echo wp_parse_url( $url, PHP_URL_HOST ).'<br>';     // example.com
          echo wp_parse_url( $url, PHP_URL_PATH ).'<br>';     // /path
          echo wp_parse_url( $url, PHP_URL_QUERY ).'<br>';    // arg=value
          echo wp_parse_url( $url, PHP_URL_FRAGMENT ).'<br><hr>'; // anchor


           get_calendar( false, true ); echo '<hr><br>';

//          $time_diff = human_time_diff( get_post_time('U'), current_time('timestamp') );
          $time_diff = human_time_diff( get_post_time( 'U', true ) );

          if( preg_match('~month|year|месяц|год|лет~iu', $time_diff ) )
	          echo "Опубликовано: ". get_the_time();
          else
	          echo "Опубликовано $time_diff назад.<br>";


          echo human_time_diff( 0, 60000 );  // 17 hour

          echo '<br>'. get_term_parents_list( 1, 'category', array(
	          'separator' => ' / ',
                  'inclusive' => true
          ) );

          ?>
          <?php get_template_part( 'layouts/preview-pdf' ); ?>


          <?php get_template_part( 'layouts/booking-widget' ); ?>
        <?php get_template_part( 'layouts/section-latest-posts' ); ?>
        <?php get_template_part( 'layouts/advertisement-content' ); ?>
        <?php get_template_part( 'layouts/section-random-pages' ); ?>
        <?php get_template_part( 'layouts/advertisement-content' ); ?>
        <?php get_template_part( 'layouts/section-random-videos' ); ?>
        <?php get_template_part( 'layouts/advertisement-content' ); ?>
      </main>
      <aside class="aside">
<!--        --><?php //get_template_part( 'layouts/advertisement-aside' ); ?>
<!--        --><?php //get_template_part( 'layouts/aside-menu-home' ); ?>
      </aside>
    </div>
  </div>

<?php get_footer();
