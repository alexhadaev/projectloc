<?php
/**
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package geo-sites
 */

get_header(); ?>

  <div class="wrapper latest-reports">

    <div class="container">
      <div class="row">
        <main class="main">
          <h2 class="section-title"><span>The Bottom Line</span></h2>
          <p class="description">Our chief editor, Cara Denney, gives a break down of this week's news and what it means for
            you.</p>
          <?php
          $currentPage = get_query_var( 'paged' );
          $args           = array(
            'post_type'      => 'report',
            'post_status'    => 'publish',
            'posts_per_page' => get_option( 'posts_per_page' ),
            'paged'          => $currentPage,
          );
          $posts          = new WP_Query( $args ); ?>
          <?php if ( $posts->have_posts() ) : ?>
            <ul class="row list">
              <?php /* Start the Loop */
              while ( $posts->have_posts() ) : $posts->the_post();
                get_template_part( 'layouts/preview-report' );
              endwhile; ?>
            </ul>

            <div class="pagination">
              <?php echo paginate_links( array(
                  'total'     => $posts->max_num_pages,
                  'prev_text' => '<span> < </span><span>' . __( 'Prev', '' ) . '</span>',
                  'next_text' => '<span>' . __( 'Next', '' ) . '</span><span> > </span>',
                  'mid_size'  => 5,
                )
              ); ?>
            </div>
            <?php wp_reset_postdata(); ?>
          <?php endif; ?>

        </main>
        <aside class="aside">
          <?php get_template_part( 'layouts/booking-widget' ); ?>
          <?php get_template_part( 'layouts/advertisement-aside' ); ?>
        </aside>
      </div>
    </div>
  </div>

<?php get_footer();
