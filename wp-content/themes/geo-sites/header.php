<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

  <meta charset="<?php bloginfo('charset'); ?>">

  <?php get_template_part( 'layouts/seo-meta' ); ?>

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- Template Basic Images Start -->
  <link rel="icon" href="<?= assets_path(); ?>/img/favicon.ico">
  <link rel="apple-touch-icon" sizes="180x180" href="<?= assets_path(); ?>/img/apple-touch-icon-180x180.png">
  <!-- Template Basic Images End -->

  <!-- Custom Browsers Color Start -->
  <meta name="theme-color" content="#123f6d">
  <!-- Custom Browsers Color End -->

  <?php wp_head(); ?>

  <!-- Url to admin-ajax.php Start -->
  <script type="text/javascript">
    var ajaxurl = "<?= admin_url('admin-ajax.php'); ?>",
      assetsurl = "<?= assets_path(); ?>";
  </script>
  <!-- Url to admin-ajax.php End -->

  <?php echo get_theme_mod('head-scripts'); ?>
</head>

<body <?php body_class(); ?>>

<header>
  <div class="login-wrapper">
    <div class="container">
        <div class="breadcrumbs"><?= do_shortcode('[breadcrumbs position=left show_home_link=1  show_current=1 ]');
	        $all_options = get_option('true_options'); // это массив
//	        var_dump($all_options);var_dump('==== ', get_option('true_options')['bc_sep']);?><!--</div>-->
      <div class="row">
        <div class="col-8 col-sm-6">
          <?php $permalink = is_front_page() ? get_home_url() : get_permalink(); ?>
          <?php if (is_user_logged_in()) : ?>
            <a href="<?php echo wp_logout_url($permalink); ?>" class="button"><i class="icon-log-out"></i><?php _e('Log out'); ?></a>
          <?php else : ?>
            <a href="<?php echo wp_login_url($permalink); ?>" class="button"><i class="icon-log-in"></i><?php _e('Log in'); ?></a>
            <a href="<?php echo wp_registration_url(); ?>" class="button"><i class="icon-register"></i><?php _e('Join'); ?></a>
          <?php endif; ?>
        </div>
        <div class="col-4 col-sm-6 social-links">
          <?php if (get_theme_mod('facebook') || get_theme_mod('twitter')) : ?>
            <span class="caption"><?php _e('Follow us on social media:'); ?></span>
            <?php if (get_theme_mod('facebook')) : ?>
              <a href="<?= get_theme_mod('facebook'); ?>" class="social-button icon-facebook"></a>
            <?php endif; ?>
            <?php if (get_theme_mod('twitter')) : ?>
              <a href="<?= get_theme_mod('twitter'); ?>" class="social-button icon-twitter"></a>
            <?php endif; ?>
          <?php endif; ?>
		  <a href="https://www.nmhco.com" style="color:white;font-weight:700;text-decoration:none;padding-left:20px;">Inquire About This Domain</a>
        </div>
      </div>
    </div>
  </div>
  <div class="hero parallax-container">
<!--    <a href="--><?php //echo get_home_url(); ?><!--" class="hero-logo"><img src="--><?//= assets_path(); ?><!--img/logo.svg" alt="--><?php //_e('Nepal.com logo') ?><!--"></a>-->
    <div class="parallax"><img src="<?= get_header_image() ?>" alt="ukraine.com"></div>
  </div>

  <nav>
    <div class="container">
      <div class="flex flex-wrap">
        <span role="button" class="btn-menu icon-menu"></span>
        <!-- The WordPress Menu goes here -->
        <?php wp_nav_menu(array(
          'theme_location' => 'header-menu',
          'menu_class' => 'navbar-nav',
          'fallback_cb' => '',
        ));
        $query = "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_wp_page_template' AND meta_value = 'templates/search-results.php' ORDER BY post_id DESC LIMIT 1";
        $search_page_id = wp_list_pluck($wpdb->get_results($query, ARRAY_A), 'post_id')[0]; ?>
        <form action="<?php the_permalink($search_page_id); ?>" method="get" class="search-form">
          <input type="text" name="q" value="<?= $_GET['q'] ?>" placeholder="Search" autocomplete="off">
          <button type="submit" class="submit-search"><span class="btn-search icon-search"></span></button>
        </form>
      </div>
    </div>
  </nav>
</header>
<?php if ( function_exists( 'bread_crumb' ) ) { bread_crumb(); } ?>
<style>
    .bread_crumb {
        margin:0;
        border-bottom:1px solid #eee;
        padding:0.8em 0 0.5em;
        clear: both;
        height: 20px;
        background: #f8f8f8;
    }
    .bread_crumb li {
        font-size:12px;
        color:rgb(180,180,180);
        float:left;
        /*margin-right:1em;*/
        list-style: none outside none;
    }
    .bread_crumb li:after {
        content : '>';
        padding-left:10px;
        margin-right:10px;
    }
    .bread_crumb li:last-child:after {
        content : '';
    }
    .bread_crumb li a {
        color:rgb(120,120,120);
    }
    .bread_crumb li.current {
    }
</style>
