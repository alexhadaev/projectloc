<?php
/**
 * Template Name: Search Results
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package geo-sites
 */

get_header(); ?>
  <div class="search-page container">
    <div class="row">
      <main class="main">
        <script>
            (function() {
                var cx = '<?php echo get_theme_mod( "cx-code"); ?>';
                var gcse = document.createElement('script');
                gcse.type = 'text/javascript';
                gcse.async = true;
                gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(gcse, s);
            })();
        </script>
        <gcse:searchresults-only></gcse:searchresults-only>
      </main><!-- #main -->
      <aside class="aside">
        <?php get_template_part( 'layouts/booking-widget' ); ?>
        <?php get_template_part( 'layouts/advertisement-aside' ); ?>
      </aside>
    </div>
  </div>
<?php
get_footer();