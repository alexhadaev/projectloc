<?php
/**
 * Template Name: City Guide
 */

get_header(); ?>

  <div class="single-page container">
    <div class="row">
		<?php while ( have_posts() ) : the_post(); ?>
      <main class="main">
		    <?php get_template_part( 'layouts/content-city-guide' ); ?>
      </main>
      <aside class="aside">
        <?php get_template_part( 'layouts/aside-menu-home' ); ?>
        <?php get_template_part( 'layouts/booking-widget' ); ?>
        <?php get_template_part( 'layouts/advertisement-aside' ); ?>
      </aside>
		<?php endwhile; // end of the loop. ?>
    </div>
  </div>

<?php get_footer();