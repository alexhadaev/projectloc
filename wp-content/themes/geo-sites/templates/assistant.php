<?php
/**
 * Template name: Assistant
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package geo-sites
 */

get_header(); ?>

  <div class="assistant-page container">
    <div class="row">
		<?php while ( have_posts() ) : the_post(); ?>
      <main class="main">
			  <?php get_template_part( 'layouts/content-pure' ); ?>
        <div class="post-footer">
          <?php echo do_shortcode('[social_warfare]'); ?>
        </div>
      </main>
      <aside class="aside">
			  <?php get_template_part( 'layouts/aside-menu-page' ); ?>
        <?php get_template_part( 'layouts/advertisement-aside' ); ?>
      </aside>
		<?php endwhile; // end of the loop. ?>
    </div>
  </div>

<?php get_footer();
