<?php
/**
 * Template name: Submit Article
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package geo-sites
 */

get_header(); ?>

  <div class="submit-article-page container">
    <div class="row">
		<?php while ( have_posts() ) : the_post(); ?>
      <main class="main">
        <?php get_template_part( 'layouts/section-add-article' ); ?>
        <div class="excerpt"><?php the_excerpt(); ?></div>
        <div class="content-text"><?php the_content(); ?></div>
      </main>
      <aside class="aside">
			  <?php get_template_part( 'layouts/aside-menu-page' ); ?>
        <?php get_template_part( 'layouts/booking-widget' ); ?>
        <?php get_template_part( 'layouts/advertisement-aside' ); ?>
      </aside>
		<?php endwhile; // end of the loop. ?>
    </div>
  </div>

<?php get_footer();
