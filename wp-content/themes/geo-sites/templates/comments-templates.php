<?php /* Markup for a single comment when inserted into the DOM */ ?>
<script type="text/html" id="tmpl-comment-single">
	<li class="{{data.status}}" id="li-comment-{{data.type}}">
		<div id="comment-{{data.id}}" class="comment">
			<div class="comment-meta comment-author vcard">
				{{data.author_name}}
				<div class="comment-meta-content">
					<cite class="fn">
						<# if ( data.link ) { #>
						<a href="{{data.link}}" rel="nofollow" class="url">
							<# } #>
							{{{data.content.rendered}}}
							<# if ( data.link ) { #>
						</a>
						<# } #>
					</cite>
					<p>
						<a href="<?php the_permalink(); ?>#comment-{{data.id}}">
							{{data.date}}
						</a>
					</p>
				</div> <!-- /comment-meta-content -->
			</div> <!-- /comment-meta -->
		</div><!-- /comment-## -->
	</li>
	<!-- #comment-## -->
</script>