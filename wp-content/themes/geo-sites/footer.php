</div><!-- #page we need this extra closing tag here -->
<div class="after-footer">
  <div class="img-wrap visible-xs"
       style="background: url('<?php echo get_theme_mod( 'footer-img-1' ) ?>') no-repeat; background-size: cover;"></div>
  <div class="img-wrap visible-xs"
       style="background: url('<?php echo get_theme_mod( 'footer-img-2' ) ?>') no-repeat; background-size: cover;"></div>
  <div class="img-wrap visible-xs"
       style="background: url('<?php echo get_theme_mod( 'footer-img-3' ) ?>') no-repeat; background-size: cover;"></div>
  <div class="img-wrap visible-sm"
       style="background: url('<?php echo get_theme_mod( 'footer-img-4' ) ?>') no-repeat; background-size: cover;"></div>
  <div class="img-wrap visible-md"
       style="background: url('<?php echo get_theme_mod( 'footer-img-5' ) ?>') no-repeat; background-size: cover;"></div>
  <div class="img-wrap visible-lg"
       style="background: url('<?php echo get_theme_mod( 'footer-img-6' ) ?>') no-repeat; background-size: cover;"></div>
</div>
<footer class="footer">
  <div class="container">

    <div class="top-block">
      <!-- The WordPress Menu goes here -->
		<?php wp_nav_menu(
			array(
				'theme_location' => 'footer-menu',
				'menu_class'     => 'navbar-nav',
				'fallback_cb'    => '',
			) ); ?>
    </div>
    <p class="copyright">© Copyright 1995-<?= date('Y') ?> <?php echo get_theme_mod( 'copyright-text' ) ?></p>
  </div>
</footer>

<div class="spinner-wrapper" style="display: none">
  <div class="spinner">
    <div class="double-bounce1"></div>
    <div class="double-bounce2"></div>
  </div>
</div>

<?php get_template_part( 'layouts/cookie-popup' ); ?>
<?php get_template_part( 'layouts/support-popup' ); ?>

<?php wp_footer(); ?>

<?php echo get_theme_mod('footer-scripts'); ?>

</body>

</html>
