<?php
$args = array(
  'post_type' => 'news',
  'posts_per_page' => 5,
  'orderby' => 'date',
  'order' => 'DESC',
  'post_status' => 'publish'
);
$news = new WP_Query($args);
$counter = 0;

if ($news->have_posts()) : ?>
  <section class="latest-news">
    <ul class="row">
      <?php while ($news->have_posts()) : $news->the_post();
        if ($counter >= 1) {
          get_template_part('layouts/preview-news');
          if ($counter == 2) {
            echo '<li class="col-12">';
            get_template_part( 'layouts/advertisement-content' );
            echo '</li>';
          }
        } else {
          get_template_part('layouts/preview-top-news');
        }
        $counter++;
      endwhile;
      echo '</ul>';
      wp_reset_query(); ?>
  </section>
<?php endif; ?>

