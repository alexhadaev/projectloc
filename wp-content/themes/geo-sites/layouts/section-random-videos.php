<?php
$args = array(
  'post_type' => 'videos',
  'posts_per_page' => 4,
  'post_status' => 'publish',
  'orderby' => 'rand',
);
$videos = new WP_Query($args);

if ($videos->have_posts()) : ?>
  <section class="random-videos">
    <h2 class="section-title"><span>Videos</span></h2>
    <ul class="row video-list">
      <?php while ($videos->have_posts()) :
        $videos->the_post();
        get_template_part('layouts/preview-video');
      endwhile;
      echo '</ul>';
      if ($videos->max_num_pages > 1) : ?>
        <div class="align-center">
          <span class="btn load-more" data-page="2">Load more</span>
        </div>
      <?php endif;
      wp_reset_query(); ?>
  </section>
<?php endif; ?>
