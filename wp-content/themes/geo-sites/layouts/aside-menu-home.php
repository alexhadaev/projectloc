<h2 class="title"><span><?= 'Top Cities' ?> </span></h2>
<?php wp_nav_menu(
	array(
		'theme_location' => 'home-aside-top-menu',
		'menu_class'     => 'aside-home-nav',
		'fallback_cb'    => '',
	) ); ?>

<h2 class="title"><span><?= 'Top Categories' ?> </span></h2>
<?php wp_nav_menu(
	array(
		'theme_location' => 'home-aside-menu',
		'menu_class'     => 'aside-home-nav',
		'fallback_cb'    => '',
	) );