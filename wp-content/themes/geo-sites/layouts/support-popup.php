<div class="support-popup" style="display: none">
  <div class="container">
    <div class="description">
      <h5>Looks like you're using an ad-blocker.</h5>
      <p>This site is supported by ads. Please help us out and disable your ad-blocker.</p>
    </div>
    <div class="buttons">
      <span class="btn" role="button">Disable</span>
      <span class="btn" role="button">Dismiss</span>
    </div>
  </div>
</div>
