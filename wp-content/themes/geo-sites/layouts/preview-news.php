<?php $img_url = get_the_post_thumbnail_url($post->ID, 'large'); ?>

<li class="col-12 col-xl-6 news-item">
  <?php if ($img_url) : ?>
    <div class="thumbnail-wrapper">
      <a class="thumbnail" href="<?php the_permalink($post->ID); ?>"
         style="background-image: url('<?= $img_url; ?>')"></a>
    </div>
  <?php endif; ?>
  <div class="content-wrapper">
    <div class="date-block">
      <?php the_time('F j, Y'); ?>
    </div>
    <h3 class="title">
      <a href="<?php the_permalink($post->ID); ?>"><?php the_title(); ?></a>
    </h3>
    <?php if (!$img_url) : ?>
      <div class="excerpt"><?php the_excerpt(); ?></div>
    <?php endif; ?>
  </div>
</li>