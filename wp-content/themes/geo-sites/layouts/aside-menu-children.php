<?php
$args = array(
  'post_parent' => $post->ID,
  'post_type' => 'any',
  'posts_per_page' => -1,
  'post_status' => 'publish',

  'meta_query' => array(
    'relation' => 'AND',
    'meta_order' => array(
      'key' => 'order_num'
    ),
    'meta_title' => array(
      'key' => 'menu_title'
    )
  ),
  'orderby' => array(
    'meta_order' => 'DESC',
    'meta_title' => 'ASC',
  )
);
$children = get_children($args);
$title = get_post_meta($post->ID, 'menu_title', true);

// Check if the post has any child
if (empty($children)) {
  $args = array(
		'post_parent' => wp_get_post_parent_id( $post->ID ),
		'post_type'   => 'any',
		'posts_per_page' => -1,
		'post_status' => 'publish',
    'meta_query' => array(
      'relation' => 'AND',
      'meta_order' => array(
        'key' => 'order_num'
      ),
      'meta_title' => array(
        'key' => 'menu_title'
      )
    ),
    'orderby' => array(
      'meta_order' => 'DESC',
      'meta_title' => 'ASC',
    )
  );
  $children = get_children($args);
  $title = get_post_meta(wp_get_post_parent_id($post->ID), 'menu_title', true);
  $title = $title != '' ? $title : 'Home';
}

if (!empty($children)) : ?>
  <div class="aside-item">
    <h2 class="title"><span><?= $title ?></span></h2>
    <ul class="link-list">
      <?php foreach ($children as $child_id => $child) { ?>
        <li <?= current_url() == get_permalink($child_id) ? 'class="current-menu-item"' : '' ?>>
          <a href="<?php the_permalink($child_id); ?>">
            <?= get_post_meta($child_id, 'menu_title', true); ?>
          </a>
        </li>
      <?php } ?>
    </ul>
  </div>
<?php endif;