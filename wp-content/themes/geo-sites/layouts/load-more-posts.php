<?php
/*
 * Load more posts
 *****************************************************************************************/

function load_posts() {
	if ( isset( $_REQUEST ) && $_REQUEST['page'] != '') {
		$page = $_REQUEST['page'];
		$args  = array(
			'post_type'      => 'blog',
			'post_status'    => 'publish',
			'posts_per_page' => 4,
			'orderby'        => 'date',
			'order'          => 'DESC',
			'paged'          => $page
		);
		$posts = new WP_Query( $args );

		if ( $posts->have_posts() ) {
			while ( $posts->have_posts() ) {
				$posts->the_post();
				get_template_part( 'layouts/preview-post' );
			}
			if ($page < $posts->max_num_pages) {
				$page++;
				echo '<li class="next-page" data-page="' . $page .'" style="display: none"></li>';
			}
			wp_reset_query();
		}
	}
	die();
}

add_action( 'wp_ajax_load_posts', 'load_posts' );
add_action( 'wp_ajax_nopriv_load_posts', 'load_posts' );