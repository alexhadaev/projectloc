<?php
$video_youtube_id = get_post_meta( $post->ID, 'video_id', true );
$img_url          = 'https://i3.ytimg.com/vi/' . $video_youtube_id . '/hqdefault.jpg';
$title            = get_the_title();
?>
<li class="col-sm-6 video-block item" data-id="<?= $post->ID ?>">
  <a class="youtube-image-container" href="<?php the_permalink( $post->ID ); ?>">
    <img src="<?= $img_url ?>" alt="<?= $title ?>">
  </a>
  <h3 class="item-title">
    <a href="<?php the_permalink( $post->ID ); ?>"><?= $title ?></a>
  </h3>
</li>