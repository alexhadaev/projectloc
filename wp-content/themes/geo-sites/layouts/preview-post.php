<?php $img = get_the_post_thumbnail( $post->ID, 'large' ); ?>

<li class="col-sm-6 post-block item">
	<?php if ( $img ) { ?>
      <a class="img-wrap" href="<?php the_permalink( $post->ID ); ?>"><?= $img; ?></a>
	<?php } ?>
  <h3 class="item-title">
    <a href="<?php the_permalink( $post->ID ); ?>"><?php the_title(); ?></a>
  </h3>
  <p class="excerpt"><?= excerpt_max_length( 200 ); ?></p>
</li>