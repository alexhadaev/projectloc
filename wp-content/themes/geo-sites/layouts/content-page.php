<?php get_template_part( 'layouts/content-pure' ); ?>

<div class="post-footer">
  <?php echo do_shortcode('[social_warfare]'); ?>
</div>

<?php get_template_part( 'layouts/advertisement-content' );

if (basename(get_page_template()) != 'assistant.php') :
  $args = array(
    'post_type' => 'page',
    'posts_per_page' => 2,
    'post_status' => 'publish',
    'orderby' => 'rand',
    'meta_query' => array(
      array(
        'key' => 'is_root_page',
        'compare' => 'NOT EXISTS'
      ),
      array(
        'key' => '_wp_page_template',
        'compare' => 'NOT EXISTS'
      ),
    ),
  );
  $pages = new WP_Query($args);

  if ($pages->have_posts()) : ?>
    <div class="related-block">
      <h3 class="section-title">
        <span>Related Post</span>
      </h3>
      <ul class="row">
        <?php while ($pages->have_posts()) {
          $pages->the_post();
          get_template_part( 'layouts/preview-page' ); ?>
        <?php }
        wp_reset_postdata(); ?>
      </ul>
    </div>
  <?php endif;
endif;
