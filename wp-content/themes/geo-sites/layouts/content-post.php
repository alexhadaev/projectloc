<div class="content">
  <?php
  $img = get_the_post_thumbnail($post->ID, 'large');
  $category = get_the_category( $post->ID );
  $category_id = $category[0]->cat_ID;
  ?>

  <h1 class="page-title"><?php the_title(); ?></h1>

  <?php if ($img) : ?>
    <div class="img-wrap"><?= $img; ?></div>
  <?php endif; ?>

<!--  <div class="excerpt">--><?php //the_excerpt(); ?><!--</div>-->

  <?php get_template_part( 'layouts/advertisement-content' ); ?>

  <div class="content-text">
    <?php the_content(); ?>
  </div>

  <?php
  $post_tags = get_the_tags();

  if ($post_tags) : ?>
    <div class="tags-block">
      <?php foreach ($post_tags as $tag) : ?>
        <a class="tag" href="<?php echo get_tag_link($tag->term_id); ?>"><?= $tag->name . ' '; ?></a>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>
<div class="post-footer">
  <?php echo do_shortcode('[social_warfare]'); ?>
</div>

<?php get_template_part( 'layouts/advertisement-content' ); ?>

<?php
$args = array(
  'post_type' => 'blog',
  'posts_per_page' => 2,
  'post_status' => 'publish',
  'post__not_in' => array($post->ID),
  'orderby' => 'rand',
  'cat' => $category_id,
);
$pages = new WP_Query($args);

if ($pages->have_posts()) : ?>
  <div class="related-block">
    <h3 class="section-title"><span>Related Post</span></h3>
    <ul class="row">
      <?php while ($pages->have_posts()) : $pages->the_post();
        get_template_part( 'layouts/preview-post' );
      endwhile;
      wp_reset_postdata(); ?>
    </ul>
  </div>
<?php endif;
