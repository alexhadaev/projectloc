<div class="content">
	<?php $video_yourtube = "https://www.youtube.com/watch?v=" . get_post_meta( $post->ID, 'video_id', true ); ?>
  <h1 class="page-title">
    <span><?php the_title(); ?></span>
  </h1>

  <div class="excerpt"><p><?= $post->post_excerpt; ?></p></div>
  <div class="video-wrap">
	  <?php echo wp_oembed_get( $video_yourtube, array( 'width' => 900 ) ); ?>
  </div>

</div>
<div class="post-footer">
	<?php echo do_shortcode( '[social_warfare]' ); ?>
</div>

<?php get_template_part( 'layouts/advertisement-content' ); ?>

<?php
$args   = array(
	'post_type'      => 'videos',
	'posts_per_page' => 2,
	'post_status'    => 'publish',
	'orderby'        => 'rand'
);
$videos = new WP_Query( $args );

if ( $videos->have_posts() ) : ?>
  <div class="related-block">
    <h3 class="section-title"><span>Related Videos</span></h3>
    <ul class="row video-list">
		<?php while ( $videos->have_posts() ) : $videos->the_post(); ?>
			<?php
			$video_yourtube_id = get_post_meta( $post->ID, 'video_id', true );
			$img_url           = 'https://i3.ytimg.com/vi/' . $video_yourtube_id . '/hqdefault.jpg';
			$title             = get_the_title();
			?>
          <li class="col-sm-6 video-block item">
            <a class="youtube-image-container" href="<?php the_permalink( $post->ID ); ?>">
              <img src="<?= $img_url ?>" alt="<?= $title ?>">
            </a>
            <h3 class="item-title">
              <a href="<?php the_permalink( $post->ID ); ?>"><?= $title ?></a>
            </h3>
          </li>
		<?php endwhile;
		wp_reset_postdata(); ?>
    </ul>
  </div>
<?php endif;
