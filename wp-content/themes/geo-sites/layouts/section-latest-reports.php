<?php
$args = array(
  'post_type' => 'report',
  'posts_per_page' => 2,
  'orderby' => 'date',
  'order' => 'DESC',
  'post_status' => 'publish'
);
$reports = new WP_Query($args);

if ($reports->have_posts()) : ?>
  <section class="latest-reports">
    <h2 class="title">The Bottom Line</h2>
    <p class="description">Our chief editor, Cara Denney, gives a break down of this week's news and what it means for
      you.</p>
    <ul class="reports">
      <?php while ($reports->have_posts()) : $reports->the_post();
        get_template_part('layouts/preview-report');
      endwhile; ?>
    </ul>
    <?php wp_reset_query(); ?>
  </section>
<?php endif; ?>

