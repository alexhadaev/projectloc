<li class="col-sm-6 page-block item">
  <?php $img = get_the_post_thumbnail($post->ID, 'medium');
  $menu_title = get_post_meta($post->ID, 'menu_title', true);
  $title = $menu_title ?: $post->post_title;
  $parent_id = wp_get_post_parent_id($post->ID);

  if ($img) : ?>
    <a class="img-wrap" href="<?php the_permalink($post->ID); ?>"><?= $img; ?></a>
  <?php endif;

  if ($parent_id > 0) :
    $category = get_post_meta($parent_id, 'menu_title', true);
    if ($category) : ?>
      <div class="categories-block">
        <span class="categories"><?= $category; ?></span>
      </div>
    <?php endif;
  endif; ?>

  <h3 class="item-title">
    <a href="<?php the_permalink($post->ID); ?>"><?= $title ?></a>
  </h3>
  <div class="excerpt"><p> <?= excerpt_max_length(400); ?></p></div>
</li>