<?php
$journalists = get_the_terms($post->ID, 'journalist');
$sources =  get_the_terms($post->ID, 'source');
$thumbnail = get_the_post_thumbnail($post->ID, 'large');
$thumbnail_caption = get_the_post_thumbnail_caption($post->ID);
?>

<div class="news-content">
  <h1 class="title"><?php the_title(); ?></h1>

  <div class="row">
    <div class="col-sm-6">
      <?php if ($journalists) : ?>
        <div class="authors">by
          <?php foreach ($journalists as $journalist) : ?>
            <span><?= $journalist->name ?></span>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>
    </div>
    <div class="date-block col-sm-6">
      <?php the_time('F j, Y'); ?>
    </div>
  </div>

  <?php if ($sources) : ?>
    <div class="source">This article originally appeared in
      <?php foreach ($sources as $source) : ?>
        <a href="<?= get_field('link_to_original') ?>"><?= $source->name ?></a>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>


  <?php if ($thumbnail) : ?>
    <div class="thumbnail-wrap">
      <?= $thumbnail; ?>
      <?php if ($thumbnail_caption) : ?>
        <p class="caption"><?= $thumbnail_caption ?></p>
      <?php endif; ?>
    </div>
  <?php endif; ?>

  <div class="excerpt"><?php the_excerpt(); ?></div>

  <?php get_template_part( 'layouts/advertisement-content' ); ?>

  <div class="content-text">
    <?php the_content(); ?>
  </div>

  <?php $post_tags = get_the_tags();

  if ($post_tags) : ?>
    <div class="tags-block">
      <?php foreach ($post_tags as $tag) : ?>
        <a class="tag" href="<?php echo get_tag_link($tag->term_id); ?>"><?= $tag->name . ' '; ?></a>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>
<div class="post-footer">
  <?php echo do_shortcode('[social_warfare]'); ?>
</div>

<?php get_template_part( 'layouts/advertisement-content' ); ?>

<?php
$args = array(
  'post_type' => 'news',
  'posts_per_page' => 2,
  'post_status' => 'publish',
  'post__not_in' => array($post->ID),
  'orderby' => 'rand'
);
$pages = new WP_Query($args);

if ($pages->have_posts()) : ?>
  <div class="related-block">
    <h3 class="section-title"><span>Related News</span></h3>
    <ul class="row">
      <?php while ($pages->have_posts()) : $pages->the_post();
        get_template_part('layouts/preview-news');
      endwhile;
      wp_reset_postdata(); ?>
    </ul>
  </div>
<?php endif;
