<div class="content">

  <h1 class="page-title"><?php the_title(); ?></h1>

  <div class="excerpt"><?php the_excerpt(); ?></div>

  <?php get_template_part( 'layouts/advertisement-content' ); ?>

  <div class="content-text">
    <?php the_content(); ?>
  </div>

  <?php
  $post_tags = get_the_tags();

  if ($post_tags) : ?>
    <div class="tags-block">
      <?php foreach ($post_tags as $tag) : ?>
        <a class="tag" href="<?php echo get_tag_link($tag->term_id); ?>"><?= $tag->name . ' '; ?></a>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>
<div class="post-footer">
  <div class="date-block">
    <span data-feather="clock"></span>
    <?php the_time('F j, Y'); ?>
  </div>
  <?php echo do_shortcode('[social_warfare]'); ?>
</div>

<?php get_template_part( 'layouts/advertisement-content' ); ?>