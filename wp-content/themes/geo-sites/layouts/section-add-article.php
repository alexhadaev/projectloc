<section class="add-article">
  <h2 class="section-title"><span>Submit an article</span></h2>
  <?php if (is_user_logged_in()) : ?>
    <form action="" id="add_article_from" method="POST">
      <div class="row">
        <div class="col-xl-8">
          <input type="text" name="article_title" id="article_title" class="required" value="" placeholder="<?php _e('Title') ?>"/>
          <textarea name="article_excerpt" id="article_excerpt" rows="3"><?php _e('Excerpt') ?></textarea>
        </div>
        <div class="col-xl-4">
          <div class="thumbnail">
            <img src="<?php echo(get_template_directory_uri()); ?>/assets/img/no-image.jpg" alt="No image">
          </div>
          <span class="thumbnail-requirements"><?php _e('You may only upload jpg, png, gif files up to 2Mb') ?></span>
          <span class="add-thumbnail-button"><?php _e('Add Thumbnail') ?></span>
          <input type="file" name="article_thumbnail" id="article_thumbnail" style="display: none"/>
        </div>
      </div>
      <?php $editor_id = 'article_content';
      $settings = array(
        'media_buttons' => false,
        'quicktags' => false,
        'tinymce' => array(
          'toolbar1' => 'formatselect,bold,italic,underline,strikethrough,bullist,numlist,blockquote,separator,alignleft,aligncenter,alignright,alignjustify,separator,undo,redo',
          'content_css' => get_template_directory_uri() . "/tinymce_custom_editor.css"
        ),
      );
      wp_editor('Text',  $editor_id, $settings);
      wp_nonce_field('post_nonce'); ?>
      <input type="hidden" name="article_author" id="article_author" value="<?= get_current_user_id(); ?>" />
      <p class="warning"><?php _e('All fields are required.'); ?></p>
      <div class="accept-terms-checkbox">
        <input type="checkbox" value="accept_terms" id="accept_terms" />
        <label for="accept_terms">I have read and accept these terms and conditions.</label>
      </div>
      <button type="submit" class="submit" disabled="disabled"><?php _e('Submit article') ?></button>
    </form>
  <?php else : ?>
    <p class="login-warning"><?php _e('Only registered users can post articles.');?></p>
    <div class="login-wrapper">
      <?php $permalink = is_front_page() ? get_home_url() : get_permalink(); ?>
      <a href="<?php echo wp_login_url($permalink); ?>" class="button"><i class="icon-log-in"></i><?php _e('Log in'); ?></a>
      <a href="<?php echo wp_registration_url(); ?>" class="button"><i class="icon-register"></i><?php _e('Register'); ?></a>
    </div>
    <?php endif; ?>
  <div class="article-send-success" style="display: none">
    <span class="icon-success"></span>
    <p class="text"><?php _e('Thanks for your article, it will be shown after verification by the moderator.') ?></p>
  </div>
  <div class="article-send-error" style="display: none">
    <span class="icon-error"></span>
    <p class="text"><?php _e('Something went wrong. Please try again later.') ?></p>
  </div>
</section>
