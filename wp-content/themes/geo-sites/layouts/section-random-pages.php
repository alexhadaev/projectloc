<?php
$args = array(
  'post_type' => 'page',
  'posts_per_page' => 8,
  'post_status' => 'publish',
  'orderby' => 'rand',
  'meta_query' => array(
    array(
      'key' => 'is_root_page',
      'compare' => 'NOT EXISTS'
    ),
    array(
      'key' => '_wp_page_template',
      'compare' => 'NOT EXISTS'
    ),
  ),
);
$pages = new WP_Query($args);

if ($pages->have_posts()) : ?>
  <section class="info-pages">
    <h2 class="section-title"><span>What to See</span></h2>
    <ul class="row pages-list">
      <?php while ($pages->have_posts()) {
        $pages->the_post();
        get_template_part('layouts/preview-page');
      } ?>
    </ul>
    <?php wp_reset_query(); ?>
  </section>
<?php endif; ?>
