<div class="content">
  <?php
  $img = get_the_post_thumbnail($post->ID, 'large');
  $parent_id = wp_get_post_parent_id($post->ID);

  if ($parent_id > 0) :
    $category = get_post_meta($parent_id, 'menu_title', true);
    if ($category) : ?>
      <div class="categories-block">
        <span class="categories"><?= $category; ?></span>
      </div>
    <?php endif;
  endif; ?>

  <h1 class="page-title"><?php the_title(); ?></h1>

  <?php if ($img) : ?>
    <div class="img-wrap"><?= $img; ?></div><?php endif; ?>

  <div class="excerpt"><?= $post->post_excerpt; ?></div>

  <?php if (basename(get_page_template()) != 'assistant.php') :
    get_template_part( 'layouts/advertisement-content' );
  endif; ?>

  <div class="content-text"><?php the_content(); ?></div>

  <?php
  $tags = get_the_tags();

  if ($tags) : ?>
    <div class="tags-block">
      <?php foreach ($tags as $tag) : ?>
        <a class="tag" href="<?php echo get_tag_link($tag->term_id); ?>"><?= $tag->name . ' '; ?></a>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>