<li class="report">
  <h3 class="title">
    <a href="<?php the_permalink($post->ID); ?>"><?php the_title(); ?></a>
  </h3>
  <div class="date-block"><?php the_time('F j, Y'); ?></div>
  <div class="excerpt"><?php the_excerpt(); ?></div>
</li>