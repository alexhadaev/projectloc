<div class="aside-item">
  <?php
  $categories = get_categories(array(
    'type' => 'blog',
    'orderby' => 'name',
    'order' => 'ASC',
    'hide_empty' => 1,
    'taxonomy' => 'language'
  ));

  if ($categories) :
    echo '<h2 class="title"><span>Categories</span></h2>';
    echo '<ul class="link-list">';
    foreach ($categories as $cat) : ?>
      <li <?= current_url() == get_category_link($cat->term_id) ? 'class="current-menu-item"' : '' ?>>
        <a href="<?= get_category_link($cat->term_id) ?>"><?= $cat->name ?></a>
      </li>
    <?php endforeach;
    echo '</ul>';
  endif; ?>
</div>
