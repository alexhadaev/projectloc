<?php $img_url = get_the_post_thumbnail_url($post->ID, 'large'); ?>

<li class="col-12 news-top">
  <div class="row news-wrapper">
    <?php if ($img_url) : ?>
      <div class="col-xl-6 col-xxl-7">
        <a class="thumbnail" href="<?php the_permalink($post->ID); ?>" style="background-image: url('<?= $img_url; ?>')"></a>
      </div>
      <div class="col-xl-6 col-xxl-5">
    <?php else : ?>
      <div class="col-12">
        <?php endif; ?>
        <div class="date-block">
         <?php the_time('F j, Y'); ?>
        </div>
        <h3 class="title">
          <a href="<?php the_permalink($post->ID); ?>"><?php the_title(); ?></a>
        </h3>
        <div class="excerpt"><?php the_excerpt(); ?></div>
        <a class="read-more" href="<?php the_permalink($post->ID); ?>"><?php _e('Read more') ?></a>
      </div>
    </div>
</li>