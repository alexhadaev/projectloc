<?php
$args = array(
  'post_type' => 'blog',
  'posts_per_page' => 4,
  'orderby' => 'date',
  'order' => 'DESC',
  'post_status' => 'publish'
);
$posts = new WP_Query($args);

if ($posts->have_posts()) : ?>
  <section class="latest-posts">
    <h2 class="section-title"><span>Latest Articles</span></h2>
    <ul class="row">
      <?php while ($posts->have_posts()) :
        $posts->the_post();
        get_template_part('layouts/preview-post');
      endwhile;
      echo '</ul>';
      if ($posts->max_num_pages > 1) : ?>
        <div class="align-center">
          <span class="btn load-more" data-page="2">Load more</span>
        </div>
      <?php endif;
      wp_reset_query(); ?>
  </section>
<?php endif; ?>
