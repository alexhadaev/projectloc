<?php
/*
 * Publish testimonial via ajax
 *****************************************************************************************/

function send_article() {
  if ( isset($_REQUEST) ) {
    $title = $_REQUEST['title'];
    $excerpt = $_REQUEST['excerpt'];
    $content = $_REQUEST['content'];
    $author = $_REQUEST['author'];
    $thumbnail = $_FILES['thumbnail'];
    $validation = true;

    if ($thumbnail['size'] > 2097152 || $title == '' || $content == '' || $excerpt == '' || $author == '') {
      $validation = false;
    }

    if ($validation) {
      $title = wp_strip_all_tags($title);
      $excerpt = wp_strip_all_tags($excerpt);

      $args = array(
        'post_title' => $title,
        'post_excerpt' => $excerpt,
        'post_content' => $content,
        'post_author' => $author,
        'post_type' => 'blog',
        'post_status'=>'pending'
      );
      $pid = wp_insert_post( $args );

      $upload_dir = wp_upload_dir();
      $upload_file = $upload_dir['path'] . '/' . basename($thumbnail['name']);
      move_uploaded_file($thumbnail['tmp_name'], $upload_file);
      $file_name = basename($upload_file);
      $wp_file_type = wp_check_filetype(basename($file_name), null);
      $attachment = array(
          'post_mime_type' => $wp_file_type['type'],
          'post_title' => preg_replace('/\.[^.]+$/', '', $file_name),
          'post_content' => '',
          'post_status' => 'inherit',
          'menu_order' => $_i + 1000
      );
      $attach_id = wp_insert_attachment($attachment, $upload_file);
      update_post_meta($pid,'_thumbnail_id', $attach_id);

      require_once( ABSPATH . 'wp-admin/includes/image.php' );

      // Создадим метаданные для вложения и обновим запись в базе данных.
      $attach_data = wp_generate_attachment_metadata( $attach_id, $upload_file );
      wp_update_attachment_metadata( $attach_id, $attach_data );

      if ($pid) {
        echo 'true';
      } else {
        echo 'false';
      }
    } else {
      echo 'false';
    }
  }

  die();
}

add_action( 'wp_ajax_send_article', 'send_article' );