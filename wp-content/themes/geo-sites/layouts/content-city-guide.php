<div class="content">
  <h1 class="page-title"><?php the_title(); ?></h1>

	<?php $img = get_the_post_thumbnail( $post->ID, 'large' ); ?>
	<?php if ( $img ) : ?>
      <div class="img-wrap"><?= $img; ?></div><?php endif; ?>

  <div class="excerpt"><?= $post->post_excerpt; ?></div>

  <div class="content-text"><?= the_content(); ?></div>

  <div id="city-guide-map" class="city-guide-map"
       data-latitude="<?php echo get_theme_mod( 'map-center-latitude' ) ?>"
       data-longitude="<?php echo get_theme_mod( 'map-center-longitude' ) ?>"
       data-zoom="<?php echo get_theme_mod( 'map-zoom' ) ?>"></div>

  <script>
//      var marker;
      // Initiate google map
      function initMap() {
          // set map position
          var googleMap = $('.city-guide-map');
          var latitude = parseFloat(googleMap.attr('data-latitude'));
          var longitude = parseFloat(googleMap.attr('data-longitude'));
          var zoom = parseInt(googleMap.attr('data-zoom'));

          var map = new google.maps.Map(document.getElementById('city-guide-map'), {
              zoom: zoom,
              center: {lat: latitude, lng: longitude},
              scrollwheel: false,
              mapTypeControl: false,
              zoomControl: true,
              zoomControlOptions: {
                  position: google.maps.ControlPosition.RIGHT_BOTTOM
              },
              scaleControl: true,
              streetViewControl: false,
              streetViewControlOptions: {
                  position: google.maps.ControlPosition.RIGHT_BOTTOM
              },
              styles: [{"featureType":"all","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]}]
          });

          google.maps.Marker.prototype.setLabel = function (label) {
              this.label = new MarkerLabel({
                  map: this.map,
                  marker: this,
                  text: label
              });
              this.label.bindTo('position', this, 'position');
          };

          var MarkerLabel = function (options) {
              this.setValues(options);
              this.span = document.createElement('span');
              this.span.className = 'map-marker-label';
          };

          MarkerLabel.prototype = $.extend(new google.maps.OverlayView(), {
              onAdd: function () {
                  this.getPanes().overlayImage.appendChild(this.span);
                  var self = this;
                  this.listeners = [
                      google.maps.event.addListener(this, 'position_changed', function () {
                          self.draw();
                      })
                  ];
              },
              draw: function () {
                  var text = String(this.get('text'));
                  var position = this.getProjection().fromLatLngToDivPixel(this.get('position'));
                  this.span.innerHTML = text;
                  this.span.style.left = position.x + 'px';
                  this.span.style.top = position.y + 'px';
              }
          });

          <?php $args = array(
            'post_type'   => 'page',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'meta_query'  => array(
              array(
                'key'     => 'is_city',
                'value'   => 1,
                'compare' => '=='
              ),
              array(
                'key'     => 'latitude',
                'value'   => '',
                'compare' => '!='
              ),
              array(
                'key'     => 'longitude',
                'value'   => '',
                'compare' => '!='
              ),
            ),
          );

          $cities = new WP_Query( $args );
          if ( $cities->have_posts() ) : $i = 0; ?>
              var cities = [
                <?php while ( $cities->have_posts() ) : $cities->the_post(); $i++;
                  echo "['" . get_post_meta( $post->ID, 'menu_title', true )
                       . "', " . get_post_meta( $post->ID, 'latitude', true )
                       . ", " . get_post_meta( $post->ID, 'longitude', true )
                       . ", " . $i . ", '" . get_permalink( $post->ID ) . "'],";
                endwhile; ?>
              ];
          <?php endif; ?>

          for (var i = 0; i < cities.length; i++) {
              var city = cities[i];
              var newMarker = new google.maps.Marker({
                  position: {lat: city[1], lng: city[2]},
                  map: map,
                  label: city[0],
                  zIndex: city[3],
                  url: city[4]
              });

              google.maps.event.addListener(newMarker, 'click', function() {
                  window.location.href = this.url;
              });
          }
      }
  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=<?= get_theme_mod('gmap-api-code'); ?>&callback=initMap"></script>

</div>

<div class="post-footer">
	<?php echo do_shortcode( '[social_warfare]' ); ?>
</div>

<?php get_template_part( 'layouts/advertisement-content' ); ?>