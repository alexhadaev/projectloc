<?php
$post_id = get_the_ID();
$seo_title = get_post_meta($post_id, 'seo_title', 1);
$seo_description = get_post_meta($post_id, 'seo_description', 1);
$seo_keywords = get_post_meta($post_id, 'seo_keywords', 1);
$thumbnail_url = get_the_post_thumbnail_url($post_id, 'large');
$image = $thumbnail_url ? $thumbnail_url : get_theme_mod( 'front-page-image' );
$archive_page =  (get_query_var('paged')) ? get_query_var('paged') : 1;

if (is_front_page()) :                                                        // Front-page
  $title = get_theme_mod('front-page-title');
  $description = get_theme_mod('front-page-description');
  $keywords = get_theme_mod('front-page-keywords');
elseif (is_post_type_archive( 'blog' )) :                           // Post archive
  $title = 'Blog archive Page ' . $archive_page;
  $description = get_theme_mod('blog-archive-description') . ' Blog archive: '
    . ucfirst(single_tag_title("", false)) . ' Page ' . $archive_page;
  $keywords = get_theme_mod('blog-archive-keywords');
elseif (is_post_type_archive( 'videos' )) :                         // Videos archive
  $title = 'Videos archive Page ' . $archive_page;
  $description = get_theme_mod('videos-archive-description') . ' Videos archive: '
    . ucfirst(single_tag_title("", false)) . ' Page ' . $archive_page;
  $keywords = get_theme_mod('videos-archive-keywords');
elseif (is_tag()) :
  $title = ucfirst(single_tag_title("", false)) . ' Tag Page ' . $archive_page;
  $description = get_theme_mod('front-page-description') . ' Tag archive: '
    . ucfirst(single_tag_title("", false)) . 'Tag Page ' . $archive_page;
  $keywords = get_theme_mod('front-page-keywords');
elseif (is_category()) :
  $title = ucfirst(single_cat_title("", false)) . ' Category Page ' . $archive_page;
  $description = get_theme_mod('front-page-description') . ' Category archive: '
    . ucfirst(single_tag_title("", false)) . 'Category Page ' . $archive_page;
  $keywords = get_theme_mod('front-page-keywords');
elseif (is_tax()) :
  $title = ucfirst(single_term_title("", false)) . ' Taxonomy Page ' . $archive_page;
  $description = get_theme_mod('front-page-description') . ' Taxonomy archive: '
    . ucfirst(single_tag_title("", false)) . ' Taxonomy Page ' . $archive_page;
  $keywords = get_theme_mod('front-page-keywords');
elseif (is_page() || is_single()) :                                           // Single page, post or video
  $title = $seo_title != '' ? $seo_title : get_the_title();
  $description = $seo_description != '' ? $seo_description : excerpt_max_length(160, '');
  $tag_list = '';
  $tags = get_the_tags();
  if ($tags) {
    foreach($tags as $tag) {
      $tag_list .= $tag->name . ',';
    }
  }
  $keywords = $seo_keywords != '' ? $seo_keywords : $tag_list;
else :                                                                        // Default
  $title = get_theme_mod('front-page-title');
  $description = get_theme_mod('front-page-description');
  $keywords = get_theme_mod('front-page-keywords');
endif; ?>

<?php if ($title) : ?>
  <title><?=  get_bloginfo() . ' | ' . $title ?></title>
  <meta property="og:title" content="<?= get_bloginfo() . ' | ' . $title ?>" />
<?php endif; ?>
<?php if ($description) : ?>
  <meta name="description" content="<?= $description ?>" />
  <meta property="og:description" content="<?= $description ?>" />
<?php endif; ?>
<?php if ($keywords) : ?>
  <meta name="keywords" content="<?= $keywords ?>" />
  <meta property="og:keywords" content="<?= $keywords ?>" />
<?php endif; ?>
<?php if ($image) : ?>
  <meta property="og:image" content="<?= $image ?>" />
<?php endif; ?>
<meta property="og:url" content="<?php the_permalink(); ?>" />
