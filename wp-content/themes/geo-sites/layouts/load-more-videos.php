<?php
/*
 * Load more videos
 *****************************************************************************************/

function load_videos() {
	if ( isset( $_REQUEST ) && $_REQUEST['page'] != '' && $_REQUEST['exclude_videos_ids'] != '') {
		$page               = $_REQUEST['page'];
		$exclude_videos_ids = $_REQUEST['exclude_videos_ids'];
		$args  = array(
			'post_type'      => 'videos',
			'posts_per_page' => 4,
			'post_status'    => 'publish',
			'orderby'        => 'rand',
			'paged'          => $page,
			'post__not_in'   => $exclude_videos_ids
		);
		$videos = new WP_Query( $args );

		if ( $videos->have_posts() ) {
			while ( $videos->have_posts() ) {
				$videos->the_post();
				get_template_part( 'layouts/preview-video' );
			}
			if ($page < $videos->max_num_pages) {
				$page++;
				echo '<li class="next-page" data-page="' . $page .'" style="display: none"></li>';
			}
			wp_reset_query();
		}
	}
	die();
}

add_action( 'wp_ajax_load_videos', 'load_videos' );
add_action( 'wp_ajax_nopriv_load_videos', 'load_videos' );