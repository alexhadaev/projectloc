<?php
// Get attractions page id
$location_id  = get_post_meta( $post->ID, 'location_id', true );
$city_page_id = $location_id != '' ? $location_id : $post->ID;
$city_title   = get_post_meta( $city_page_id, 'menu_title', true );

global $wpdb;
$attractions_page_ids = $wpdb->get_results( "select post_id from $wpdb->postmeta where meta_value = 'Attractions'", ARRAY_A );
$attractions_page_id  = $attractions_page_ids[0]['post_id'];

$args       = array(
	'post_type'   => 'page',
	'posts_per_page' => -1,
	'post_status' => 'publish',
	'meta_query'  => array(
		array(
			'key'     => 'location_id',
			'value'   => $city_page_id,
			'compare' => '=='
		),
	)
);
$city_pages = new WP_Query( $args );

if ( $city_pages->have_posts() ) : ?>
  <h2 class="city-title"><span><?= $city_title ?> </span></h2>
<?php endif;

$args              = array(
	'post_parent' => $attractions_page_id,
	'post_type'   => 'page',
	'posts_per_page' => -1,
	'post_status' => 'publish'
);
$attractions_pages = new WP_Query( $args );

if ( $attractions_pages->have_posts() ) :
	while ( $attractions_pages->have_posts() ) : $attractions_pages->the_post();
		$args  = array(
			'post_parent' => $post->ID,
			'post_type'   => 'page',
			'posts_per_page' => -1,
			'post_status' => 'publish',
			'meta_query'  => array(
				array(
					'key'     => 'location_id',
					'value'   => $city_page_id,
					'compare' => '=='
				),
			),
			'order'       => 'ASC',
			'orderby'     => 'meta_value',
			'meta_key'    => 'menu_title'
		);
		$pages = new WP_Query( $args );

		if ( $pages->have_posts() ) : ?>
          <div class="aside-item">
            <h3 class="title"><span><?= get_post_meta( $post->ID, 'menu_title', true ); ?> </span></h3>
            <ul class="link-list">
            <?php while ( $pages->have_posts() ) :
              $pages->the_post(); ?>
              <li <?= current_url() == get_permalink($post->ID) ? 'class="current-menu-item"' : '' ?>>
                <a href="<?php the_permalink( $post->ID ); ?>">
                  <?= get_post_meta( $post->ID, 'menu_title', true ); ?>
                </a>
              </li>
            <?php endwhile;
            wp_reset_postdata(); ?>
            </ul>
          </div>
		<?php endif;
	endwhile;
	wp_reset_postdata();
endif;