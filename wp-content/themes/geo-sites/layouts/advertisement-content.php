<?php if (get_theme_mod('ad_728x90')) : ?>
  <div class="advertise-content size-728x90"><?= get_theme_mod('ad_728x90') ?></div>
<?php endif; ?>
<?php if (get_theme_mod('ad_300x250')) : ?>
  <div class="advertise-content size-300x250"><?= get_theme_mod('ad_300x250') ?></div>
<?php endif; ?>