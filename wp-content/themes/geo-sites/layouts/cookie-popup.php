<?php
  $link_to_terms = get_permalink(get_page_by_path( 'corporate/terms-of-use'));
?>

<div class="cookie-popup" style="display: none">
  <div class="container">
    <p class="description">We are using cookies to make the website better. By clicking Agree you are accepting <a href="<?= $link_to_terms ?>">Terms of Service</a></p>
    <span class="btn accept" role="button">Got it!</span>
  </div>
</div>
