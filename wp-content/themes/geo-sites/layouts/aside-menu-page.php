<?php
$is_city     = get_post_meta( $post->ID, 'is_city', true );
$location_id = get_post_meta( $post->ID, 'location_id', true );

if ( $is_city || $location_id ) {
	get_template_part( 'layouts/aside-menu-city' );
} else {
	get_template_part( 'layouts/aside-menu-children' );
}